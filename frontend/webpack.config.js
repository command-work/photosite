const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');


module.exports = {
  mode: 'development',
  entry: {
    bundle: path.join(__dirname, '/src/index.js'),
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name].js',//,//.[contenthash]
    publicPath: '/'
  },
  devServer: {
    host: '0.0.0.0',
    before: (app, server) => {
      // Support for cases when page loaded on other pathname than /.
      // Without it we got 404 when trying to open paths like /topics.
        app.get("*", (req, res, next) => {
          console.log(req.url);
          if ( ! (req.url.endsWith(".svg")
              || req.url.endsWith(".png")
              || req.url.endsWith(".js")
              || req.url.endsWith(".ico")
              || req.url.endsWith(".ttf")
              || req.url.endsWith(".woff")
              || req.url.endsWith(".woff2")
              || req.url.endsWith(".js.map")
              || req.url.endsWith(".json")) ) {
            req.url = "/"
          } else {
            console.log("!");
          }

          const returnJSonPaths = [

          ];
          returnJSonPaths.forEach( (item, index) => {
            if ( req.url.startsWith(item) ) {
              req.url = req.url.replace(item, "")
            }
          });
          next("route");
        });
    }
  },

  module: {
    rules: [
      {
        test: /\.(css)$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(sa|sc)ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
      },

      {

      },

      {
        test: /\.pug$/,
        use: [
          'pug-loader'
        ]
      },
      // static
      {
        test: /\.(png|ico|svg|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          context: 'src',
          name(resourcePath, resourceQuery) {
            return '[path][name].[ext]';
          },
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '/src/views/index.html'),
      filename: 'index.html'
    }),
    new webpack.ProvidePlugin({
      '$':          'jquery',
      '_':          'lodash',
      'ReactDOM':   'react-dom',
      'cssModule':  'react-css-modules',
      'Promise':    'bluebird'
    }),
  ]
};
