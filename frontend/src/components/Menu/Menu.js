import * as React from "react";
import logoImage from '../../images/logo.svg';
import searchImage from '../../images/menu/findIcon.svg'

import chatsImage from '../../images/menu/chatsIcon.svg';
import notifImage from '../../images/menu/notifIcon.svg';
import profileImage from '../../images/menu/profileIcon.svg';
import plusImage from '../../images/menu/plusIcon.svg';
import settingsImage from  '../../images/menu/settingsIcon.svg'
import RegModal from "../RegModal";
import LoginModal from "../LoginModal";
import Api from "../../utils/Api";
import {AddSmthModal} from "../AddSmthModal/AddSmthModal";
import CustomButton from "../button/button";
import {Link, NavLink, Redirect} from "react-router-dom";


class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowReg: false,
            isShowLogin: false,
            isShowAdd: false,
            auth: false,
            searchType: 'typePinSearch',
            mostType: 'mostNothingSearch',
            timeType: 'allTimeSearch',
            sortType: 'normalSortSearch',
            query: '',
            redirect: '',
        };

        Api.isAuth()
            .then(res => res.json())
            .then(result => {
                    if (result && result.message && !this.more) {
                        this.setState({
                            error: result.message,
                            isLoaded: true,
                        });

                    } else if (result.id > 0) {
                        this.state.auth = true;
                        this.setState(this.state);
                    }
                },
                (error) => {
                    this.setState({
                        error: "Авторизация не удалась! Все пропало! Деньжки тю-тю",
                        isLoaded: true,
                    });
                }
            );
    }

    // openRegModal = () => {
    //     let oldState = this.state;
    //     oldState.isShow = true;
    //     this.setState(oldState)
    // };

    loginFunc = () => {
        this.state.isShowReg = false;
        this.state.isShowLogin = true;
        this.setState(this.state);
    };

    regFunc = () => {
        this.state.isShowReg = true;
        this.state.isShowLogin = false;
        this.setState(this.state);
    };

    closeFunc = () => {
        this.state.isShowReg = false;
        this.state.isShowLogin = false;
        this.state.auth = true;
        this.setState(this.state);
    };

    openAddModal = () => {
        this.setState({
            isShowReg: false,
            isShowLogin: false,
            isShowAdd: true,
        })
    };

    closeAddModal = () => {
        this.setState({
            isShowReg: false,
            isShowLogin: false,
            isShowAdd: false,
        })
    };

    onSearchChange(event) {
        this.setState({
            query: event.target.value,
        })

    }

    search() {
        let  request = '/search?query=' + this.state.query;

        if (this.state.searchType === 'typePinSearch') {
            request += '&what=pin';
        }
        if (this.state.searchType === 'typeUserSearch') {
            request += '&what=user';
        }
        if (this.state.mostType === 'mostCommentSearch') {
            request += '&most=commented';
        }

        if (this.state.mostType === 'mostNothingSearch') {
            request += '&most=popular';
        }


        if (this.state.timeType === 'daySearch') {
            request += '&date=day';
        }


        if (this.state.timeType === 'weekSearch') {
            request += '&date=week';
        }


        if (this.state.timeType === 'monthSearch') {
            request += '&date=month';
        }


        if (this.state.sortType === 'normalSortSearch') {
            request += '&desc=true';
        }

        if (this.state.sortType === 'reverseSortSearch') {
            request += '&desc=false';
        }
        this.setState({
            redirect: request,
        })
    }


    settingsClick(event) {
        console.log(event.target);
        const target = event.target;
        const targetId = target.id;
        if (targetId === 'typePinSearch' || targetId === 'typeUserSearch') {
            this.setState({
                searchType: targetId
            });
            target.classList.add('checked');
            if (targetId === 'typePinSearch') {
                document.getElementById("typeUserSearch").classList.remove('checked');
            } else {
                document.getElementById("typePinSearch").classList.remove('checked');
            }
        }
        const mostIds = ['mostCommentSearch', 'mostNothingSearch'];
        const isMost = mostIds.indexOf(targetId);
        for (const id of mostIds) {
            if (targetId === id) {
                this.setState({
                    mostType: targetId
                });
                target.classList.add('checked');
            } else if (isMost >= 0) {
                document.getElementById(id).classList.remove('checked');
            }
        }

        const timeIds = ['daySearch', 'weekSearch', 'monthSearch', 'allTimeSearch']
        const isTime = timeIds.indexOf(targetId);
        for (const id of timeIds) {
            if (targetId === id) {
                this.setState({
                    timeType: targetId
                });
                target.classList.add('checked');
            } else if (isTime >= 0) {
                document.getElementById(id).classList.remove('checked');
            }
        }

        const sortIds = ['normalSortSearch', 'reverseSortSearch'];
        const isSort = sortIds.indexOf(targetId);
        for (const id of sortIds) {
            if (targetId === id) {
                this.setState({
                    sortType: targetId
                });
                target.classList.add('checked');
            } else if (isSort >= 0) {
                document.getElementById(id).classList.remove('checked');
            }
        }
    }

    render() {
        const modalParams = {
            text:"",
            funcParam: null,
            isShowReg: this.state.isShowReg,
            isShowLogin: this.state.isShowLogin,
            loginFunc: this.loginFunc,
            regFunc: this.regFunc,
            closeFunc: this.closeFunc
        };

        const {isShowAdd, redirect} = this.state;

        if (redirect !== '') {
            this.setState({
                redirect: ''
            })
            return (
                <Redirect to={redirect}/>
            )
        }

        let thirdAuthPart = <div className="buttons">
            <a id="loginModal" onClick={this.loginFunc.bind(this)}>
                <button>Войти</button>
            </a>
            <a id="regModal" onClick={this.regFunc.bind(this)}>
                <button >Регистрация</button>
            </a>
        </div>;

        const id = localStorage.getItem('user_id');

        if (this.state.auth === true || id) {
            thirdAuthPart = <div className="icons">
                <NavLink to="/chats">
                    <img src={chatsImage}/>
                </NavLink>
                <NavLink to="/notifications">
                    <img src={notifImage}/>
                </NavLink>
                <NavLink to={"/profile/" + window.localStorage.getItem('user_id')} >
                    <img src={profileImage}/>
                </NavLink>
                <a onClick={this.openAddModal.bind(this)}>
                    <img src={plusImage}/>
                </a>
            </div>;
        }


        return (
            <React.Fragment>
                <AddSmthModal isOpen={isShowAdd} closeModal={() => this.closeAddModal()}/>
                <RegModal modalParams={modalParams}/>
                <LoginModal modalParams={modalParams}/>
                <div className="menu">
                    <div className="logo">
                        <NavLink to="/" id="logoNavLink">
                            <img src={logoImage} width="190px"/>
                        </NavLink>
                    </div>
                    <div className="find">
                        <div className="find_pin">
                            <div className="settingsMenu">
                                <img src={settingsImage} id="settingsBtn"/>
                                <div className="settingsWindow">
                                    <p className="title">Что ищем</p>
                                    <p className="variant checked" id='typePinSearch' onClick={this.settingsClick.bind(this)}>Пины</p>
                                    <p className="variant" id='typeUserSearch' onClick={this.settingsClick.bind(this)}>Пользователей</p>
                                    <div className="additionalParams" id='additionalParams'>
                                        <p className="title">Самые</p>
                                        <p className="variant" id='mostCommentSearch' onClick={this.settingsClick.bind(this)}>Обсуждаемые</p>
                                        <p className="variant checked" id='mostNothingSearch' onClick={this.settingsClick.bind(this)}>Популярные</p>

                                        <p className="title">Время</p>
                                        <p className="variant" id='daySearch' onClick={this.settingsClick.bind(this)}>За день</p>
                                        <p className="variant" id='weekSearch' onClick={this.settingsClick.bind(this)}>За неделю</p>
                                        <p className="variant" id='monthSearch' onClick={this.settingsClick.bind(this)}>За месяц</p>
                                        <p className="variant checked" id='allTimeSearch' onClick={this.settingsClick.bind(this)}>За все время</p>

                                        <p className="title">Порядок</p>
                                        <p className="variant checked" id='normalSortSearch'  onClick={this.settingsClick.bind(this)}>Обычный</p>
                                        <p className="variant" id='reverseSortSearch'  onClick={this.settingsClick.bind(this)}>Обратный</p>
                                    </div>
                                </div>
                            </div>
                            <input id="searchInput" type="text" className="search_input" onChange={this.onSearchChange.bind(this)}/>
                            <CustomButton onClick={this.search.bind(this)}><img id="searchBtn" src={searchImage}/></CustomButton>
                        </div>
                    </div>
                        <div className="thirdpart">
                            {thirdAuthPart}
                        </div>
                </div>
                </React.Fragment>
        );
    }
}

export default Menu;
