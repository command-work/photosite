import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useHistory
} from "react-router-dom";

import PinView from "../../views/pin-view/pin-view";
import PinCreationView from "../../views/pin-creation/pin-creation";
import FeedPage from "../../views/FeedPage";

import Modal from 'react-modal';

import Menu from "../Menu";
import {SearchView} from "../../views/search-view/search-view";
import NotifPage from "../../views/NotifPage";


Modal.setAppElement('#root');
const NavRoute = ({exact, path, component: Component}) => (
    <Route exact={exact} path={path} render={(props) => (
        <div>
            <Menu/>
            <Component {...props}/>
        </div>
    )}/>
)


class App extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Router>
                    <Switch>
                        <NavRoute path="/pin/creation" component={PinCreationView}/>
                        <NavRoute path="/pin/:pinId" component={PinView}/>
                        <NavRoute path="/search" component={SearchView}/>
                        {/*<NavRoute path="/profile/:userId" component={UserView}/>*/}
                        <NavRoute exact path="/notifications" component={NotifPage}/>
                        <NavRoute exact path="/" component={FeedPage}/>
                    </Switch>
                </Router>
            </React.Fragment>
        );
    }
}

export default App
