import React from "react";


const CustomButton = (props) => {
    return (
        <button className={'button-' + props.type + ' custom-button'} onClick={(event) => props.onClick(event)}>{props.children}</button>
    )
};

export default CustomButton;