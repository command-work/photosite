import * as React from "react";

export const SearchUser = ({avatar, id, login}) => {
    return (
        <a href={'/user/' + id} className={"search-user"}>
            <img alt='user-photo' src={avatar}/>
            <div className={'user-name'}> {login} </div>
        </a>
    )
};
