import * as React from "react";
import logoImage from '../../images/logo.svg';
import Api from "../../utils/Api";
import {validators} from '../../utils/validation';

class LoginModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowLogin: this.props.modalParams.isShowLogin,
            infoModalMessage: "",
            closeFunc: this.props.modalParams.closeFunc
        }

        this.csrf_flag = false;
    }

    loginSend = () => {
        const login = document.getElementById('loginUser').value;
        const pass = document.getElementById('passUser').value;

        const login_valid = validators.username(login);
        const pass_valid = validators.password(pass);

        if (!login_valid) {
            this.state.infoModalMessage = 'Логин должен состоять из трех и более символов,\n' +
                'и содержать только латинские буквы, цифры и подчеркивание';
            this.setState(this.state)
        } else if (!pass_valid) {
            this.state.infoModalMessage = 'Пароль должен состоять из шести или более символов';
            this.setState(this.state)
        } else {
            console.log(login, pass);
            Api.login(login, pass)
                .then(res => res.json())
                .then(result => {
                        if (result && result.message) {
                            this.setState({
                                error: result.message,
                                isLoaded: true,
                            });
                            if (result.message === 'no csrf-token found' && !this.csrf_flag) {
                                this.csrf_flag = true;
                                this.loginSend();
                            } else {
                                this.state.infoModalMessage = 'Неверный логин или пароль';
                                this.setState(this.state);
                            }
                        } else {
                            localStorage.setItem('token_jwt', result['token_jwt']);
                            localStorage.setItem('user_id', result['id']);
                            this.more = true;
                            this.setState((state) => ({
                                error: null,
                                isLoaded: true,
                            }));
                            this.state.closeFunc();
                        }
                    },
                    (error) => {
                        this.state.infoModalMessage = 'Неверный логин или пароль';
                        this.setState(this.state);
                        // this.setState({
                        //     error: "Авторизация не удалась! Все пропало! Деньжки тю-тю",
                        //     isLoaded: true,
                        // });
                    }
                );
        }
    };

    render(props) {
        const isShow = this.props.modalParams.isShowLogin === undefined ? false : this.props.modalParams.isShowLogin;
        return (isShow && <div className='modal'>
            <div className='background'/>
            <div className='window'>
                <img src={logoImage} alt="Фотографикс" width="140px"/>
                <div className='title'>
                    Авторизация
                </div>
                <div id="infoModalMessage">
                    {this.state.infoModalMessage}
                </div>
                <div className="line">
                    <input id="loginUser" type="text" placeholder="Логин"/>
                </div>
                <div className="line">
                    <input id="passUser" type="text" placeholder="Пароль"/>
                </div>
                <a id="sendLogin">
                    <input type="submit" className="red_btn" value="Войти" onClick={this.loginSend.bind(this)}/>
                </a>
                <a id="textLoginModal">
                    <div className="mini_text" onClick={this.props.modalParams.regFunc}>
                        Еще не зарегистрировались в Фотографиксе?<br/> Регистрация
                    </div>
                </a>
            </div>
        </div>);
    }
};

export default LoginModal
