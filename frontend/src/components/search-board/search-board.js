import * as React from "react";

export const SearchBoard = ({image, id, name}) => {
    return (
        <a href={'/?filter=board&board_id=' + id} className={"search-board"}>
            <img alt='board-photo' src={image}/>
            <div className={'board-name'}> {name} </div>
        </a>
    )
};
