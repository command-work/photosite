import * as React from "react";
import Modal from 'react-modal';
import {Link} from "react-router-dom";
import Api from "../../utils/Api";
import CustomButton from "../button/button";




const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        border: 'unset',
        borderRadius : '5px',
    }
};


export class AddSmthModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            state: null,
        };
    }

    componentDidMount() {
    }

    onCreateBoardState() {
        this.setState({
            state: 'creation',
        })
    }


    onCreate() {
        const form = document.forms.boardcreate;
        const data = {
            name: form.name.value,
            description: form.description.value
        };

        console.log(data);
        Api.createBoard(data)
            .then(res => res.json())
            .then(result => {
                console.log(result);
                if (result && result.id > 0) {
                    this.props.closeModal();
                    this.setState({
                        state: null,
                    });
                    window.location.href = `/?filter=board&board_id=${result.id}`;
                }
            })
    }

    closeModal() {
        this.setState({
            state: null
        });
        this.props.closeModal()
    }


    render() {

        const {state} = this.state;
        return (
            <Modal
                isOpen={this.props.isOpen}
                onRequestClose={this.props.closeModal}
                style={customStyles}
                overlayClassName={'overlay-boards'}
                contentLabel="Example Modal"
            >
                { state === 'creation'
                    ?
                    <div className={'board-create-modal'}>
                        <h1> Создание доски </h1>
                        <form name={'boardcreate'} className={'pin-edit'}>
                            <input name={'name'} maxLength={30}/>
                            <textarea name={'description'} className={'pin-text'} maxLength={256}/>
                        </form>
                        <div className={'buttons-group'}>
                            <CustomButton type={'active'} onClick={this.onCreate.bind(this)}> Добавить </CustomButton>
                            <CustomButton onClick={() => this.closeModal()}> Отменить </CustomButton>
                        </div>
                    </div>
                    :
                    <div className={'add-smth-modal'}>
                        <h1> Что вы хотите создать? </h1>
                        <div className={'buttons-group'}>
                            <a href={'/pin/creation'}><CustomButton type={'active'}>Пин</CustomButton></a>
                            <CustomButton type={'active'} onClick={this.onCreateBoardState.bind(this)}>Доску</CustomButton>
                        </div>
                    </div>

                }
            </Modal>

        );
    }
}
