import * as React from "react";
import logoImage from '../../images/logo.svg';
import Api from "../../utils/Api";
import {validators} from '../../utils/validation';

class RegModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowReg: this.props.modalParams.isShowReg,
            infoModalMessage: "",
            closeFunc: this.props.modalParams.closeFunc
        }
        this.csrf_flag = false;
    }

    regSend = () => {
        const email = document.getElementById('emailUser').value;
        const login = document.getElementById('loginUser').value;
        const pass = document.getElementById('passUser').value;

        const email_valid = validators.email(email);
        const login_valid = validators.username(login);
        const pass_valid = validators.password(pass);

        if (!email_valid) {
            this.state.infoModalMessage = 'Введите корректный email';
            this.setState(this.state)
        } else if (!login_valid) {
            this.state.infoModalMessage = 'Логин должен состоять из трех и более символов,\n' +
                'и содержать только латинские буквы, цифры и подчеркивание';
            this.setState(this.state)
        } else if (!pass_valid) {
            this.state.infoModalMessage = 'Пароль должен состоять из шести или более символов';
            this.setState(this.state)
        } else {
            Api.createUser(email, login, pass)
                .then(res => res.json())
                .then(result => {
                        if (result && result.message) {
                            this.setState({
                                error: result.message,
                                isLoaded: true,
                            });
                            if (result.message === 'no csrf-token found' && !this.csrf_flag) {

                                this.csrf_flag = true;
                                this.regSend();
                            }

                        } else {
                            // console.log(result['token_jwt']);
                            // console.log(result['id']);
                            localStorage.setItem('token_jwt', result['token_jwt']);
                            localStorage.setItem('user_id', result['id']);
                            this.more = true;
                            this.setState((state) => ({
                                error: null,
                                isLoaded: true,
                            }));
                            this.state.closeFunc();
                        }
                    },
                    (error) => {
                        console.log("Регистрация не удалась! Все пропало! Деньжки тю-тю");
                        this.setState({
                            error: "Регистрация не удалась! Все пропало! Деньжки тю-тю",
                            isLoaded: true,
                        });
                    }
                );
        }
    };

    render(props) {
        const isShow = this.props.modalParams.isShowReg === undefined ? false : this.props.modalParams.isShowReg;
        return (isShow && <div className='modal'>
            <div className='background'/>
            <div className='window'>
                <img src={logoImage} alt="Фотографикс" width="140px"/>
                <div className='title'>
                    Регистрация
                </div>
                <div id="infoModalMessage">
                    {this.state.infoModalMessage}
                </div>
                <div className="line">
                    <input id="emailUser" type="text" placeholder="Почта"/>
                </div>
                <div className="line">
                    <input id="loginUser" type="text" placeholder="Логин"/>
                </div>
                <div className="line">
                    <input id="passUser" type="text" placeholder="Пароль"/>
                </div>
                <a id="sendReg">
                    <input type="submit" className="red_btn" value="Регистрация" onClick={this.regSend.bind(this)}/>
                </a>
                <a id="textLoginModal">
                    <div className="mini_text" onClick={this.props.modalParams.loginFunc}>
                        Уже есть аккаунт в Фотографиксе?<br/> Войти
                    </div>
                </a>
            </div>
        </div>);
    }
};

export default RegModal
