import * as React from "react";
import {Link} from "react-router-dom";

const Pin = ({image, id}) => {
    return (
        <Link to={'/pin/' + id} >
            <div className={"pin"}>
            <img alt='pin-photo' src={image}/>
            </div>
        </Link>
    )
}

export default Pin
