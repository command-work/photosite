const MY_PATH = 'http://84.252.142.112/api';

window.getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
};

/**
 * Creates a new Network object
 * @class
 * @type {Network}
 */
export default class Network {

    /**
     * GET request
     * @static
     * @param {string} path
     * @returns {Promise<Response>}
     */
    static doGet(path = '/') {
        const token = localStorage.getItem('token_jwt');
        const csrfCookie = document.cookie.match(/csrf_token=(\w+)/);
        if (csrfCookie) {
            return fetch(MY_PATH + path, {
                method: 'GET',
                mode: 'cors',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'X-CSRF-TOKEN': csrfCookie[0],
                    'Authorization': 'Bearer '+ token,
                },
            });
        }
        return fetch(MY_PATH + path, {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Authorization': 'Bearer '+ token,
            }
        });
    }

    /**
     * POST request
     * @static
     * @param {string} path
     * @param {Object} body
     * @returns {Promise<Response>}
     */
    static doPost(path = '/', body = {}) {
        const csrfCookie = document.cookie.match(/csrf_token=(\w+)/);
        const token = localStorage.getItem('token_jwt');

        console.log(window.getCookie(''));
        if (csrfCookie) {
            return fetch(MY_PATH + path, {
                method: 'POST',
                mode: 'cors',
                credentials: 'include',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'X-CSRF-TOKEN': csrfCookie[0],
                    'Authorization': 'Bearer '+ token,

                },
            });
        }
        return fetch(MY_PATH + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+ token,
            },
        });
    }

    /**
     * POST form-data request
     * @param {string} path
     * @static
     * @param {Object} formData
     * @returns {Promise<Response>}
     */
    static doPostFormData(path = '/', formData) {
        const csrfCookie = document.cookie.match(/csrf_token=(\w+)/);
        console.log(csrfCookie);
        const token = localStorage.getItem('token_jwt');
        if (csrfCookie) {
            return fetch(MY_PATH + path, {
                method: 'POST',
                mode: 'cors',
                credentials: 'include',
                body:  formData,
                headers: {
                    'X-CSRF-TOKEN': csrfCookie[0],
                    'Authorization': 'Bearer '+ token,
                },
            });
        }
        return fetch(MY_PATH + path, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            body: formData,
            headers: {
                'Authorization': 'Bearer '+ token,
            }
        });
    }

    /**
     * DELETE request
     * @static
     * @param {string} path
     * @returns {Promise<Response>}
     */
    static doDelete(path = '/', body = {}) {
        return fetch(MY_PATH + path, {
            method: 'DELETE',
            mode: 'cors',
            credentials: 'include',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+ token,
		'X-CSRF-TOKEN': getCookie('csrf_token'),
            },
        });
    }


    /**
     * PUT request
     * @static
     * @param {string} path
     * @param {Object} body
     * @returns {Promise<Response>}
     */
    static doPut(path = '/', body = {}) {
        const token = localStorage.getItem('token_jwt');
        return fetch(MY_PATH + path, {
            method: 'PUT',
            mode: 'cors',
            credentials: 'include',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-CSRF-TOKEN': getCookie('csrf_token'),
                'Authorization': 'Bearer '+ token,
            },
        });
    }

    /**
     * PATCH request
     * @static
     * @param {string} path
     * @param {Object} body
     * @returns {Promise<Response>}
     */
    static doPatch(path = '/', body = {}) {
        const token = localStorage.getItem('token_jwt');
        return fetch(MY_PATH + path, {
            method: 'PATCH',
            mode: 'cors',
            credentials: 'include',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Bearer '+ token,
		'X-CSRF-TOKEN': getCookie('csrf_token'),
            },
        });
    }

    /**
     * PUT form-data request
     * @param {string} path
     * @param {Object} formData
     * @returns {Promise<Response>}
     */
    static doPutFormData(path = '/', formData) {
        const token = localStorage.getItem('token_jwt');
        return fetch(MY_PATH + path, {
            method: 'PUT',
            mode: 'cors',
            credentials: 'include',
            body: formData,
            headers: {
                'Authorization': 'Bearer '+ token,
		'X-CSRF-TOKEN': getCookie('csrf_token'),
            }
        });
    }
}
