import Network from './Network';

/**
 * Creates a new API object
 * Класс реализует взаимодействие Api c бекендом
 * @class
 * @type {Api}
 */
export default class Api {

    static createUser(email, login, password) {
        return Network.doPost('/users', {
            email,
            login,
            password,
        });
    }

    static getUser(id) {
        return Network.doGet(`/users/${id}`);
    }

    static changeUserInfo(id, diff) {
        return Network.doPatch(`/users/${id}`, diff);
    }

    static changeUserPassword(id, password) {
        return Network.doPut(`/users/${id}/password`, {
            password
        });
    }

    static changeUserAvatar(id, image) {
        const formData = new FormData();
        formData.append('image', image);
        return Network.doPutFormData(`/users/${id}/avatar`, formData);
    }

    static subscribeToUser(id, targetId) {
        return Network.doPost(`/users/${id}/subscriptions?target_id=${targetId}`);
    }

    static unsubscribeFromUser(id, target_id) {
        return Network.doDelete(`/users/${id}/subscriptions?target_id=${targetId}`);
    }

    static getSubscriptions(id, start=0, limit=10) {
        return Network.doGet(`/users/${id}/subscriptions?start=${start}&limit=${limit}`);

    }

    static getSubscribers(id, start=0, limit=10) {
        return Network.doGet(`/users/${id}/subscribers?start=${start}&limit=${limit}`);
    }

    static getBoards(id, start=0, limit=10) {
        return Network.doGet(`/users/${id}/boards?start=${start}&limit=${limit}`);
    }

    static getChats(id, start=0, limit=10) {
        return Network.doGet(`/users/${id}/chats?start=${start}&limit=${limit}`);
    }

    static getChatsMessages(id, chatId,start=0, limit=10) {
        return Network.doGet(`/users/${id}/chats/${chatId}/messages?start=${start}&limit=${limit}`);
    }

   // --------------------------------------------------------

    static createPin({name, description, userId, boardId, image}) {
        return Network.doPost(`/pins`, {
            name,
            description,
            user_id: userId,
            board_id: boardId,
            image,
        })
    }

    static getPinsList(filter, start=0, limit=10, boardId = null, userId = null) {
        let path = `/pins?start=${start}&limit=${limit}`;
        if (filter) {
            path += `&filter=${filter}`
        }
        if (boardId && filter === 'board') {
            path += `&id=${boardId}`
        }

        if (userId && filter === 'user') {
            path += `&id=${userId}`
        }
        return Network.doGet(path);
    }

    static getPin(id) {
        return Network.doGet(`/pins/${id}`);
    }

    static changePin(id, diff) {
        return Network.doPatch(`/pins/${id}`,  diff);
    }

    static deletePin(id) {
        return Network.doDelete(`/pins/${id}`);
    }

    static createComment(pinId, comment, userId) {
        return Network.doPost(`/pins/${pinId}/comments`, {
            comment,
            user_id: userId,
        })
    }

    static getComment(pinId, commentId) {
        return Network.doGet(`/pins/${pinId}/comments/${commentId}`)
    }

    static getCommentsList(id, start=0, limit=10) {
        return Network.doGet(`/pins/${id}/comments?start=${start}&limit=${limit}`);
    }
    // -----------------------

    static addPinToBoard(id, pinId) {
        return Network.doPost(`/boards/${id}/pins?pin_id=${pinId}`);
    }

    static createBoard({name, description}) {
        return Network.doPost(`/boards`, {
            name,
            description,
        });
    }

    static changeBoard(id, {name, description}) {
        return Network.doPatch(`/boards/${id}`, {
            name, description,
        })
    }

    static deleteBoard(id) {
        return Network.doDelete(`/boards/${id}`)
    }

    // ----------------

    static login(login, password) {
        return Network.doPost(`/auth`, {
            login, password
        })
    }

    static isAuth() {
        return Network.doGet(`/auth`)
    }

    static logout() {
        return Network.doDelete(`/auth`)
    }

    //------------------

    static search({start, limit, what, description, date, desc, most }) {
        let path = `/search?limit=${limit}&start=${start}`;
        let data = {
            what, description, date, desc, most
        };
        Object.keys(data).forEach((val) => {
            if (data[val]) {
                path += `&${val}=${data[val]}`;
            }
        });

        return Network.doGet(path);
    }
}
