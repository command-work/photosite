const regExpressions = {
    email: /^[\w-.]{1,}@[\w-.]{1,}$/,
    username: /^[a-zA-Z0-9_]{3,}$/,
    // ASCII chars from ! to ~
    password: /[!-~]{6,}/,
    userLink: /^\/user\/[\d-.]{1,}$/,
    pinLink: /^\/pin\/[\d-.]{1,}$/,
    pinsUserLink: /^\/pins\/user\/[\d-.]{1,}$/,
    deskUserLink: /^\/desk\/[\d-.]{1,}$/,
    chatUserLink: /^\/chats\/user\/[\d-.]{1,}$/,
    searchLink: /^\/search$/
};

export const validators = {
    email: (email) => validateField(email, regExpressions.email),
    username: (username) => validateField(username, regExpressions.username),
    password: (password) => validateField(password, regExpressions.password),
    userLink: (userLink) => validateField(userLink, regExpressions.userLink),
    pinLink: (pinLink) => validateField(pinLink, regExpressions.pinLink),
    pinsUserLink: (pinsUserLink) => validateField(pinsUserLink, regExpressions.pinsUserLink),
    deskUserLink: (deskUserLink) => validateField(deskUserLink, regExpressions.deskUserLink),
    chatUserLink: (chatUserLink) => validateField(chatUserLink, regExpressions.chatUserLink),
    searchLink: (searchLink) => validateField(searchLink, regExpressions.searchLink)
};

const validateField = (field, regExp) => {
    return regExp.test(String(field));
};
