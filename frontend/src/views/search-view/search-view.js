import * as React from "react";
import Pin from "../../components/Pin";
import Api from "../../utils/Api";
import CustomButton from "../../components/button/button";
import {SearchUser} from "../../components/search-user/search-user";

export class SearchView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: [],
        };
        this.start = 0;
        this.limit = 10;
        this.more = false;
        this.filter = '';
        this.board_id = 0;
        this.user_id = 0;
    }

    componentDidMount() {
        this.loadMore()
    }

    loadMore() {
        const searchParams = new URLSearchParams(this.props.location.search);
        this.params = {
            what: searchParams.get('what'),
            description: searchParams.get('query'),
            date: searchParams.get('date'),
            desc: searchParams.get('desc') || true,
            most: searchParams.get('most'),
            start: this.start,
            limit: this.limit
        };

        Api.search(this.params)
            .then(res => res.json())
            .then(result => {
                    if (result && result.message && !this.more) {
                        this.setState({
                            error: result.message,
                            isLoaded: true,
                        });

                    } else if (!result || !result.message) {
                        this.start += this.limit;
                        this.more = true;
                        this.setState((state) => ({
                            error: null,
                            isLoaded: true,
                            data: state.data.concat(result || []) || [],
                        }));
                    }
                },
                (error) => {
                    this.setState({
                        error: "Не  удалось получить результат!",
                        isLoaded: true,
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, data} = this.state;
        if (error) {
            return (
                <div> {error} </div>
            )
        }
        if (!isLoaded) {
            return (
                <div>Loading...</div>
            )
        }

        return (
            <div className={'feed-page'}>
                <div className={'feed-page-title'}>
                    Результаты поиска
                </div>
                {(this.params.what === 'pin' || !this.params.what) &&
                <div className={'pin-list'}>
                    {data.map((pin) => (
                            <Pin image={pin.image} id={pin.id} />
                        )
                    )}
                </div>
                }
                {(this.params.what === 'user') &&
                <div className={'pin-list'}>
                    {data.map((user) => (
                            <SearchUser avatar={user.avatar} id={user.id} login={user.login}/>
                        )
                    )}
                </div>
                }
                {data.length > 0 && data.length % this.limit === 0 &&
                <CustomButton type={"active"} onClick={this.loadMore.bind(this)}>Загрузить еще</CustomButton>}
                {data.length === 0 &&
                <div>Упс! Ничего не найдено! </div>}
            </div>
        )
    }
}
