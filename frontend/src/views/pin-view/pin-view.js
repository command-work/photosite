import React from 'react';
import Pin from "./components/pin/pin";
import Api from "../../utils/Api";
import Modal from 'react-modal'
import BoardsModal from "./components/boards-modal/boards-modal";


const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        border: 'unset',
        borderRadius : '5px',
    }
};

class PinView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoadedPin: false,
            isLoadedComments: false,
            pin: {},
            comments: [],
            modalIsOpen: false,
            options: [],
            auth: false,
        };
        this.id = props.match.params.pinId;
        this.subtitle = null;
    }

    componentDidMount() {
        Api.getPin(this.id)
            .then(res => res.json())
            .then(
                (data) => {
                    if (data && data.message) {
                        this.setState({
                            isLoadedPin: true,
                            error: data.message
                        }
                    )
                    } else {
                        this.setState({
                            isLoadedPin: true,
                            pin: data,
                            error: null
                        });
                    }
                },
                (error) => {
                    console.log(error);
                    this.setState({
                        isLoadedPin: true,
                        error: 'Не удалось загрузить пин, пожалуйста, попробуйте позже',
                    })
                },
            );
       this.getComments();
       this.getBoards();
    }


    getComments() {
        Api.getCommentsList(this.id)
            .then(res => res.json())
            .then(
                (result) => {
                    if (result && result.message) {
                        // this.setState({
                        //         isLoadedComments: true,
                        //         error: result.message
                        //     }
                        // )
                    } else {
                        this.setState({
                                isLoadedComments: true,
                                comments: result || [],
                                error: null
                            }
                        )
                    }
                },
                (error) => {
                    console.log(error);
                    this.setState({
                        isLoadedComments: true,
                        error: 'Не удалось загрузить пин, пожалуйста, попробуйте позже',
                    })
                }
            );
    }

    getBoards() {
        const id = localStorage.getItem('user_id');

        if (id) {
            Api.getBoards(id,0, 1000)
                .then(res => res.json())
                .then(result => {
                    const options = result.map((board) => {
                        return {
                            value: board.id,
                            label: board.name,
                        }
                    });
                    this.setState({
                        options: options,
                        auth: true,
                    })
                })
        } else {
            this.setState({
                auth: false,
            })
        }
    }

    createComment(text) {
        const id = localStorage.getItem('user_id');
        Api.createComment(
            this.id,
            text, parseInt(id))
            .then(res => this.getComments());
    }


    openModal() {
        this.setState({
            modalIsOpen: true
        });
    }

     closeModal(){
        this.setState({
            modalIsOpen: false
        });
    }

    saveOnBoard(boardId) {
        console.log(boardId);
        Api.addPinToBoard(boardId, this.id)
            .then(res => res.json())
            .then(result => {
                console.log('super!');
            });


        this.closeModal()
    }

    editPin(pin) {
        Api.changePin(this.id, pin)
            .then(res => res.json())
            .then(result => {
                this.setState({
                    pin: result,
                })
            })
    }


    render() {
        const {error, isLoadedPin, isLoadedComments, pin, comments, modalIsOpen, options, auth} = this.state;
        console.log(error);
        if (error) {
            return (<div> {error} </div>)
        } else if (!isLoadedPin) {
            return (<div> loading....</div>)
        }



        return (
            <div className="pin-view">
                <Pin
                    pin={pin}
                    comments={comments}
                    createComment={this.createComment.bind(this)}
                    onSave={this.openModal.bind(this)}
                    onEdit={this.editPin.bind(this)}
                    auth={auth}
                />
                <div>
                    <Modal
                        isOpen={modalIsOpen}
                        onRequestClose={this.closeModal.bind(this)}
                        style={customStyles}
                        overlayClassName={'overlay-boards'}
                        contentLabel="Example Modal"
                    >

                        <BoardsModal
                            onClose={this.closeModal.bind(this)}
                            options={options}
                            onAdd={this.saveOnBoard.bind(this)}
                        />
                    </Modal>
                </div>
            </div>

        );
    }
}

export default PinView
