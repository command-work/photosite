import React, {useState} from "react";
import CustomButton from "../../../../components/button/button";
import profileIcon from "../../../../images/menu/profileIcon.svg";
import editIcon from "../../../../images/edit.svg";


const PinInfo = (props) => {
    const pin = props.pin || null;
    if (!pin )
        return (<div>ERROR! NO PIN DATA </div>);

    let  [edit, setEdit] = useState(false);

    const onEdit = () => {
        setEdit(true);
    };

    const editPin = () => {
        const form = document.forms.pinedit;


        const data = {
            name: form.name.value,
            description: form.description.value,
        };

        props.onEdit(data);
    };

    return (
        <div className="pin-info">
            <div className="pin-actions">
                <div className="pin-user">
                    <img src={profileIcon} alt={'photo'}/>
                    <div className="pin-user-name">{pin.user && pin.user.login || 'anon'}</div>
                </div>
                {props.auth &&
                <div className={'pin-action-buttons'}>
                    <CustomButton onClick={onEdit.bind(this)}><img src={editIcon} alt={'photo'}/></CustomButton>
                    <CustomButton
                        type={'active'}
                        onClick={props.onSave}
                    > Сохранить</CustomButton>
                </div>
                }
            </div>
            { edit
                ? <>
                <form name={'pinedit'} className={'pin-edit'}>
                    <input name={'name'}  defaultValue={pin.name} maxLength={30}/>
                    <textarea name={'description'} className={'pin-text'} defaultValue={pin.description} maxLength={256}/>
                </form>
                <CustomButton onClick={()=>editPin()} type={'active'}>Сохранить изменения</CustomButton>
            </>
                :
                <>
                    <h1 className="pin-title">{pin.name}</h1>
                    <div className="pin-text">{pin.description}</div>
                </>
            }

        </div>
    )
};

export default PinInfo
