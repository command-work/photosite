import React from "react";
import CustomButton from "../../../../components/button/button";
import PinComment from "../pin-comment/pin-comment";
import PinInfo from "../pin-info/pin-info";
import backButton from '../../../../images/backBtn.svg';
import shareIcon from '../../../../images/sendIcon.svg';
import {Link} from "react-router-dom";

class Pin extends React.Component {
    render() {
        const pin = this.props.pin || null;
        const comments = this.props.comments || [];
        if (!pin )
            return (<div>ERROR! NO PIN DATA </div>);

        const onChange = (event) => {
            this.commentText = event.target.value;
            };
        const createComment = (event) => {
            document.getElementById('comment-text-input').value=''
            this.props.createComment(this.commentText || '');
        };

        return (
            <div className="pin-full">
                <div className={'pin-back-button'}> <Link to={'/'}><img src={backButton} alt={'back'}/> </Link></div>
                <div className={'pin-block'}>

                <div className="pin-image">
                    <img src={pin.image} alt={'pin-photo'}/>
                </div>

                <div className="pin-data">
                        <PinInfo
                            pin={pin}
                            onSave={this.props.onSave}
                            auth={this.props.auth}
                            onEdit={this.props.onEdit}
                        />
                        <div className="pin-comments-block">
                            {comments.map((comment) => {
                                return ( <PinComment key={comment.id} user={comment.user} text={comment.comment}/>)
                            })}
                        </div>
                    {this.props.auth &&
                    <div className='pin-create-comment'>
                        <input type={'text'} id='comment-text-input' onKeyPress={onChange}/>
                        <CustomButton onClick={(event) => createComment(event)}><img src={shareIcon}
                                                                   alt={'send'}/></CustomButton>
                    </div>
                    }

                </div>
                </div>
            </div>
        )
    }
}

export default Pin;
