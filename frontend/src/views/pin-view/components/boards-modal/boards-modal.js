import React from "react";
import Modal from "react-modal";
import Select from "react-select";
import CustomButton from "../../../../components/button/button";

export default class BoardsModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedOption: null,
        }
    }

    handleChange = selectedOption => {
        this.setState({ selectedOption });
    };

    onSavePin () {
        this.props.onAdd(this.state.selectedOption && this.state.selectedOption.value || null)
    }

    render() {
        const { selectedOption} = this.state;


        return (
            <div className={'boards-modal'}>
                <h1> Выбор доски </h1>
                <Select
                    name={"board_id"}
                    placeholder={'Выберите доску...'}
                    className={'select-board'}
                    classNamePrefix={'select-board'}
                    value={selectedOption}
                    onChange={this.handleChange}
                    options={this.props.options}
                />
                <div className={'buttons-group'}>
                <CustomButton type={'active'} onClick={this.onSavePin.bind(this)}> Добавить </CustomButton>
                    <CustomButton onClick={this.props.onClose}> Отменить </CustomButton>
                </div>
            </div>
        )

    }
}
