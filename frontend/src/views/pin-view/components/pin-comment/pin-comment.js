import React from "react";
import profileIcon from "../../../../images/menu/profileIcon.svg"


const PinComment = ({user, text}) => {
    return (
        <div className="pin-comment">
            <img src={user.avatar || profileIcon} alt={'photo'}/>
            <div className='pin-comment-value'>
                <div className="pin-comment-name">{user.login}</div>
                <div className="pin-comment-text">{text} </div>
            </div>
        </div>
    )
};

export default PinComment;
