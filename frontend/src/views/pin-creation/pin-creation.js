import React from 'react';
import backButton from "../../images/backBtn.svg";
import CustomButton from "../../components/button/button";
import Api from "../../utils/Api";
import Select from "react-select";
import {Link, Redirect} from "react-router-dom";

class PinCreationView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            avatar: '',
            options: [],
            auth: true,
            selectedOption: null,
            redirect: '',
        };
    }

    componentDidMount() {
        const id = localStorage.getItem('user_id');
        console.log(id)
        if (id) {
            Api.getBoards(id,0, 1000)
                .then(res => res.json())
                .then(result => {
                    const options = result.map((board) => {
                        return {
                            value: board.id,
                            label: board.name,
                        }
                    });
                    this.setState({
                        options: options,
                        auth: true,
                    })
                })
        } else {
            this.setState({
                auth: false,
            })
        }


    }

    onCreatePin(event) {
        const form = document.forms.pindata;
        const id = localStorage.getItem('user_id');
        const data = {
            name: form.name.value,
            description: form.description.value,
            boardId: this.state.selectedOption && this.state.selectedOption.value,
            userId: parseInt(id),
            image: this.state.avatar
        };

        if (!data.name || !data.description  || data.image === '') {
            return;
        }

        Api.createPin(data)
            .then(result =>
                result.json())
            .then( (res) => {
                console.log(res);
                const id = parseInt(res);
                if ( id ) {
                    this.setState({
                        redirect: '/pin/' + id,
                    })
                }
            });
    }



    onEditImageButton()  {
        const imageInput = document.querySelector('.js-image-input');
        imageInput.click()
    }

    onEditImage() {
        const imageInput = document.querySelector('.js-image-input');

        this.getBase64(imageInput)
    }

    getBase64(input) {
        let file = input.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload =  () => {
            this.setState({
                avatar: reader.result,
            });
        };
        reader.onerror = (error) => {
            console.log('Error: ', error);
        };
    }

    handleChange = selectedOption => {
        this.setState({ selectedOption });
    };


    render() {
        const {avatar, options, auth, selectedOption, redirect} = this.state;

        if (!auth) {
            return (
                <Redirect to={'/'}/>
            )
        }

        if (redirect !== '') {
            return (
                <Redirect to={redirect}/>
            )
        }

        return (
            <div className="pin-view">
                <div className="pin-full">
                    <div className={'pin-back-button'}><Link to={'/'}><img src={backButton} alt={'back'}/> </Link></div>
                    <div className={'pin-creation-block'}>
                        <div className={'pin-creation-title'}> Создание пина</div>

                        <div className={'pin-creation-body'}>
                            <div className={'pin-creation-image'}>
                                <div className="image">
                                    <img src={avatar} alt={'pin-photo'}/>
                                </div>
                                <CustomButton type={'active'} onClick={this.onEditImageButton.bind(this)}> Добавить изображение </CustomButton>
                                <input className='js-image-input' type={'file'} hidden
                                       onChange={this.onEditImage.bind(this)}/>
                            </div>
                            <div className="pin-creation-data">
                            <form className="pin-creation-data" name={'pindata'}>
                                <input name={"name"} placeholder={"Название пина..."} maxLength={30}/>
                                <textarea name={"description"} rows={12} placeholder={'Описание пина... (необзательно)'} maxLength={256}/>
                                <Select name={"board_id"}
                                        placeholder={'Выберите доску..'}
                                        className={'select-board'}
                                        classNamePrefix={'select-board'}
                                        value={selectedOption}
                                        onChange={this.handleChange}
                                        options={options}/>
                            </form>
                            <CustomButton type={'active'} onClick={this.onCreatePin.bind(this)}> Создать </CustomButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PinCreationView
