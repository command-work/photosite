import * as React from "react";
import Pin from "../../components/Pin";
import Api from "../../utils/Api";
import CustomButton from "../../components/button/button";

class FeedPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            pins: [],
        };
        this.start = 0;
        this.limit = 10;
        this.more = false;
        this.filter = '';
        this.board_id = 0;
        this.user_id = 0;
    }

    componentDidMount() {
        this.loadMore()
    }

    loadMore() {
        const filter = new URLSearchParams(this.props.location.search).get("filter");
        this.filter = filter;
        const user_id = new URLSearchParams(this.props.location.search).get("user_id");
        this.user_id = user_id;
        const board_id = new URLSearchParams(this.props.location.search).get("board_id");
        this.board_id = board_id;
        Api.getPinsList(filter || 'main', this.start, this.limit, board_id, user_id)
            .then(res => res.json())
            .then(result => {
                    if (result && result.message && !this.more) {
                        this.setState({
                            error: result.message,
                            isLoaded: true,
                        });

                    } else if (!result || !result.message) {
                        this.start += this.limit;
                        this.more = true;
                        this.setState((state) => ({
                            error: null,
                            isLoaded: true,
                            pins: state.pins.concat(result) || [],
                        }));
                    }
                },
                (error) => {
                    this.setState({
                        error: "Не  удалось получить пины!",
                        isLoaded: true,
                    });
                }
            );
    }

    render() {
        const {error, isLoaded, pins} = this.state;
        if (error) {
            return (
                <div> {error} </div>
            )
        }
        if (!isLoaded) {
            return (
                <div>Loading...</div>
            )
        }

        return (
            <div className={'feed-page'}>
                <div className={'feed-page-title'}>
                    {(this.filter === 'main' || this.filter === '') && <> Лента пинов </>}
                    {(this.filter === 'board') && <> Доска {this.board_id} </>}
                    {(this.filter === 'user') && <> Лента пользователя {this.user_id} </>}
                    {(this.filter === 'sub') && <> Лента подписок </>}

                </div>

                <div className={'pin-list'}>
                    {pins.map((pin) => (
                            <Pin image={pin.image} id={pin.id} />
                        )
                    )}
                </div>
                {pins.length > 0 && pins.length % this.limit === 0 &&
                <CustomButton type={"active"} onClick={this.loadMore.bind(this)}>Загрузить еще</CustomButton>}
                {pins.length === 0 &&
                <div>Упс! Ничего не найдено! </div>}
            </div>
        )
    }
}


export default FeedPage;
