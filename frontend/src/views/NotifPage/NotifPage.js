import * as React from "react";
import Pin from "../../components/Pin";
import Api from "../../utils/Api";
import CustomButton from "../../components/button/button";
import logoImage from "../../images/logo.svg";
import backBtn from "../../images/backBtn.svg"
import NotifItem from "./NotifItem";
import notifAvatar from "../../images/1.png";

class NotifPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            pins: [],
        };
    }

    componentDidMount() {
        this.getNotif()
    }

    getNotif() {

    }

    backFunc() {
        window.history.back();
    }

    render() {
        const itemParams = {
            text:"",
            id:1,
            avatarImage: notifAvatar,
            notifText: "Новый комментарий от testuser на ваш пин “Савеловская”: очень круто!"
        };
        return (
            <div className='page'>
                <div className='page_content'>
                    <div className='backProfileLink' id="backProfileLink" onClick={this.backFunc}>
                        <img src={backBtn}/>
                    </div>
                   <h1 className="page_title">Уведомления</h1>
                    <div className='notif_block' id="notifBlock">
                        <div id="infoPageMessage">

                        </div>
                        <NotifItem itemParams={itemParams}/>
                    </div>
                </div>
            </div>
        )
    }
}


export default NotifPage;
