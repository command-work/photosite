import * as React from "react";
import {Link} from "react-router-dom";

class NotifItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render(props) {
        return (
            <div className='notif_item'>
                <div className='avatar_img'>
                    <Link to={'/profile/'+this.props.itemParams.id}>
                        <img src={this.props.itemParams.avatarImage}/>
                    </Link>
                </div>
                <div className='notif_text'>
                    {this.props.itemParams.notifText}
                </div>
            </div>
        )
    }
}


export default NotifItem;
