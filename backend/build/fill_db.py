from random import random, randint, sample, choice, gauss

import psycopg2
from faker import Faker
from psycopg2 import sql

MAX_DATE = 1.5e9
fake = Faker()


def main():
    conn = psycopg2.connect(dbname='photosite', user='totw',
                            password='photosite', host='localhost')
    c = conn.cursor()

    create_users(c, 100)
    create_boards(c)
    create_pins(c)
    create_tags(c)
    create_comments(c)
    create_subs(c)

    c.close()
    conn.commit()
    conn.close()
    return


def create_users(cursor, request_cnt):
    if request_cnt <= 0:
        return 0

    dels = ["", "_", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    result_cnt = 0
    for i in range(request_cnt):
        username = fake.word() + choice(dels) + fake.word()
        email = fake.email()
        password = fake.word()
        avatar = ''
        about = fake.sentence()
        created = randint(1, MAX_DATE)
        try:
            cursor.execute("insert into users values(default, %s, %s, %s, %s, %s, 0,0, %s);",
                           [email, username, password, about, avatar, created])
            result_cnt += 1
        except:
            print("it was an error while creating users")
            print("count: ", result_cnt)
            break

    print("Filling users table completed")
    return result_cnt


def create_boards(cursor):
    user_ids, users_cnt = get_id_list(cursor, "users")
    result_cnt = 0
    for uid in user_ids:
        cnt = randint(1, 5)
        for i in range(cnt):
            name = fake.word() + " " + fake.word()
            descr = fake.sentence()
            created = randint(1, MAX_DATE)
            try:
                cursor.execute("insert into boards values(default, %s, %s, %s, %s, true);",
                            [uid, name, descr, created])
                result_cnt += 1
            except:
                print("it was an error while creating boards")
                print("user: ", uid)
                break
    print("Filling boards table completed")
    return result_cnt

def create_pins(cursor):
    user_ids, users_cnt = get_id_list(cursor, "users")
    board_ids, boards_cnt = get_id_list(cursor, "boards")
    result_cnt = 0
    for uid in user_ids:
        cnt = randint(5, 20)
        for i in range(cnt):
            name = fake.word() + " " + fake.word()
            descr = fake.sentence()
            created = randint(1, MAX_DATE)
            views = randint(0, users_cnt)
            image = ''
            try:
                cursor.execute("insert into pins values(default, %s, %s, %s, %s, %s, %s, 0);",
                            [uid, name, descr, image, created, views])
                result_cnt += 1
            except:
                print("it was an error while creating pins")
                print("user: ", uid)
                break
    print("Filling pins table completed")
    for i in range(result_cnt):
        cnt = randint(1, 10)
        bids = sample(board_ids, cnt)
        for bid in bids:
            original = True if randint(0, 1) else False
            try:
                cursor.execute("insert into boards_pins values(%s, %s, %s);", [i + 1, bid, original])
            except:
                print("it was an error while creating pins")
                print("pin and board: ", i + 1, bid)
                break
    print("Filling board_pins table completed")
    return result_cnt

def create_tags(cursor):
    pin_ids, pin_cnt = get_id_list(cursor, "pins")
    result_cnt = randint(10, pin_cnt)
    dels = ["", "_", "-", " "]
    for i in range(result_cnt):
        tag = fake.word() + choice(dels) + fake.word()
        try:
            cursor.execute("insert into tags values(default, %s);", [tag])
        except:
            print("it was an error while creating tags")
            print("tag: ", i + 1)
            break
    print("Filling tags table completed")

    tag_ids, tag_cnt = get_id_list(cursor, "tags")
    for pid in pin_ids:
        cnt = randint(0, 3)
        tids = sample(tag_ids, cnt)
        for tid in tids:
            try:
                cursor.execute("insert into pins_tags values(%s, %s);", [pid, tid])
            except:
                print("it was an error while creating tags")
                print("tag and pin: ", tid, pid)
                break
    print("Filling pins_tags table completed")
    return result_cnt

def create_comments(cursor):
    user_ids, users_cnt = get_id_list(cursor, "users")
    pin_ids, pin_cnt = get_id_list(cursor, "pins")
    result_cnt = 0
    for pid in pin_ids:
        cnt = randint(0, users_cnt // 5)
        users = sample(user_ids, cnt)
        for uid in users:
            text = fake.sentence()
            created = randint(1, MAX_DATE)
            try:
                cursor.execute("insert into comments values(default, %s, %s, %s, %s);", [uid, pid, text, created])
                result_cnt += 1
            except:
                print("it was an error while creating comments")
                print("user and pin: ", uid, pid)
                break
    print("Filling comments table completed")
    return result_cnt

def create_subs(cursor):
    user_ids, users_cnt = get_id_list(cursor, "users")
    result_cnt = 0
    for uid in user_ids:
        cnt = randint(0, users_cnt)
        subs = sample(user_ids, cnt)
        for sid in subs:
            try:
                cursor.execute("insert into subscriptions values(default, %s, %s);", [sid, uid])
                result_cnt += 1
            except:
                print("it was an error while creating subscriptions")
                print("user and sub_user: ", uid, sid)
                break
    print("Filling subscriptions table completed")
    return result_cnt


def get_id_list(cursor, table_name):
    cursor.execute(sql.SQL("select id from {};").format(sql.Identifier(table_name)))
    ids = cursor.fetchall()
    id_list = [ids[i][0] for i in range(len(ids))]
    return id_list, len(id_list)


main()