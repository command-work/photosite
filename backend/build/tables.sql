-- Database: photosite

DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS pins CASCADE;
DROP TABLE IF EXISTS boards CASCADE;
DROP TABLE IF EXISTS tags CASCADE;
DROP TABLE IF EXISTS pins_tags CASCADE;
DROP TABLE IF EXISTS boards_pins CASCADE;
DROP TABLE IF EXISTS subscriptions CASCADE;
DROP TABLE IF EXISTS boards_pins CASCADE;
DROP TABLE IF EXISTS notifies CASCADE;;
DROP TABLE IF EXISTS chats CASCADE;;
DROP TABLE IF EXISTS chat_messages CASCADE;
DROP TABLE IF EXISTS comments CASCADE;


CREATE TABLE users (
    id serial PRIMARY KEY,
    email text NOT NULL,
    login text UNIQUE NOT NULL,
    password bytea NOT NULL,
    about text,
    avatar text,
    subscriptions int not null,
    subscribers int not null,
    created_at int NOT NULL
);

CREATE TABLE boards (
    id serial PRIMARY KEY,
    user_id int,
    name text NOT NULL,
    description text,
    created_at int not null,
    editable boolean,
    constraint to_user foreign key (user_id) references users(id) on delete set null
);

CREATE TABLE pins (
    id serial PRIMARY KEY,
    user_id int,
    name text,
    description text,
    image text NOT NULL,
    created_at int not null,
    views int NOT NULL,
    comments int NOT NULL,
    constraint to_user foreign key (user_id) references users(id) on delete set null
);

create table tags (
    id serial primary key,
	tag text not null
);

create table pins_tags (
    pin_id int,
    tag_id int,
    UNIQUE (pin_id, tag_id),
    constraint to_pin foreign key (pin_id) references pins(id) on delete cascade,
    constraint to_tag foreign key (tag_id) references tags(id) on delete cascade
);

CREATE TABLE boards_pins (
    pin_id int,
    board_id int,
    original boolean not null,
    UNIQUE (pin_id, board_id),
    constraint to_pin foreign key (pin_id) references pins(id) on delete cascade,
    constraint to_board foreign key (board_id) references boards(id) on delete cascade
);


CREATE TABLE subscriptions (
    id serial PRIMARY KEY,
    user_id int,
    subscribed_at int,
    UNIQUE (user_id, subscribed_at),
    constraint to_user foreign key (user_id) references users(id) on delete set null,
    constraint to_user2 foreign key (subscribed_at) references users(id) on delete set null
);

CREATE TABLE comments (
    id serial PRIMARY KEY,
    user_id int,
    pin_id int,
    comment text not null,
    created_at int not null,
    constraint to_user foreign key (user_id) references users(id) on delete set null,
    constraint to_pin foreign key (pin_id) references pins(id) on delete cascade
);

CREATE TABLE notifies (
    id serial PRIMARY KEY,
    user_id int REFERENCES users(id) NOT NULL,
    message text NOT NULL,
    from_user_id int REFERENCES users(id) NOT NULL,
    created_at int not null
);

/*
CREATE FUNCTION new_desks() RETURNS trigger AS $trigger_bound$
BEGIN
	INSERT INTO boards(user_id, name, description, created_at)
	VALUES(NEW.id, 'my pins', 'pins', NEW.created_at)
	RETURNING NEW;
END;
$trigger_bound$
LANGUAGE plpgsql;


CREATE TRIGGER new_user_desk AFTER INSERT
ON users
EXECUTE FUNCTION new_desks();
 */

CREATE TABLE chats (
    id serial PRIMARY KEY,
    sender_id int REFERENCES users(id) NOT NULL,
    receiver_id int REFERENCES users(id) NOT NULL
);

CREATE TABLE chat_messages (
    id serial PRIMARY KEY,
    sender_id int REFERENCES users(id) NOT NULL,
    receiver_id int REFERENCES users(id) NOT NULL,
    message text,
    sticker text,
    created_at int not null
);

CREATE OR REPLACE FUNCTION make_tsvector(name TEXT)
    RETURNS tsvector AS $$
BEGIN
    RETURN (setweight(to_tsvector('russian', name),'A') ||
            setweight(to_tsvector('english', name), 'B'));
END
$$ LANGUAGE 'plpgsql' IMMUTABLE;

create or replace function add_tag(updated_pin_id int, new_tag text)  returns int as $$
declare
	new_tag_id int;
begin
	insert into tags(tag) (
		select new_tag where
		not exists (select tag from tags
					where tag=new_tag));

	select id from tags
	where tag = new_tag
	into new_tag_id;

	insert into pins_tags(pin_id, tag_id) (
		select updated_pin_id, new_tag_id
		where not exists(
			select 1 from pins_tags
			where pin_id = updated_pin_id and tag_id = new_tag_id));

	return new_tag_id;
end
$$ LANGUAGE 'plpgsql';