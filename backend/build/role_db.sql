drop role if exists totw;
create role totw with password 'photosite';
alter role totw with login superuser;

-- DROP DATABASE photosite;

CREATE DATABASE photosite
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;