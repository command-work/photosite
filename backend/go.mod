module photosite

go 1.16

require (
	github.com/golang/mock v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/robbert229/jwt v2.0.0+incompatible
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
)
