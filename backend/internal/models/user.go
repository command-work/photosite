// Модель для сущности пользователя
package models

import "photosite/internal/pkg/utils"

type User struct {
	ID            int    `json:"id"`
	Email         string `json:"email,omitempty"`
	Password      []byte `json:"-"`
	Login         string `json:"login"`
	About         string `json:"about,omitempty"`
	Avatar        string `json:"avatar,omitempty"`
	Subscriptions int    `json:"subscriptions"`
	Subscribers   int    `json:"subscribers"`
	TokenJWT      string `json:"token_jwt,omitempty"`
	IsSub         int    `json:"is_subscribed,omitempty"`
}

type AuthUser struct {
	Email    string `json:"email,omitempty" valid:"email,length(1|50)"`
	Login    string `json:"login" valid:"alphanum,length(1|20)"`
	Password string `json:"password" valid:"length(6|100)"`
}

var JWTKey = utils.RandString(32)
