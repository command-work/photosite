// Модель для сущности "доски" изображений
package models

type Board struct {
	Id          int    `json:"id"`
	UserId      int    `json:"user_id,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description"`
	LastPin     *Pin   `json:"last_pin,omitempty"`
}

type InputBoard struct {
	Name        string `json:"name" valid:"length(1|60), required"`
	Description string `json:"description" valid:"length(0|1000), required"`
}
