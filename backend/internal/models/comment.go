// Модель для сущности комментария к изображениям
package models

import "time"

type CommentWithUser struct {
	ID        int       `json:"id"`
	FullUser  *User     `json:"user"`
	PinID     int       `json:"pin_id"`
	Comment   string    `json:"comment"`
	CreatedAt time.Time `json:"created_at"`
}

type Comment struct {
	ID        int       `json:"id"`
	UserID    int       `json:"user_id"`
	PinID     int       `json:"pin_id"`
	Comment   string    `json:"comment"`
	CreatedAt time.Time `json:"created_at"`
}
