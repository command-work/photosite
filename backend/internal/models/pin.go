// Модель для сущности "пина" - изображения
package models

import "time"

type PinWithUser struct {
	ID          int       `json:"id"`
	FullUser    *User     `json:"user"`
	BoardID     int       `json:"board_id"`
	Name        string    `json:"name"`
	Image       string    `json:"image"`
	Description string    `json:"description,omitempty"`
	CreatedAt   time.Time `json:"created_at"`
	Tags        []string  `json:"tags"`
	Views       int       `json:"views"`
	Comments    int       `json:"comments"`
}

type Pin struct {
	ID          int       `json:"id"`
	UserID      int       `json:"user_id"`
	BoardID     int       `json:"board_id"`
	Name        string    `json:"name"`
	Image       string    `json:"image"`
	Description string    `json:"description,omitempty"`
	CreatedAt   time.Time `json:"created_at"`
	Tags        []string  `json:"tags"`
	Views       int       `json:"views"`
	Comments    int       `json:"comments"`
}
