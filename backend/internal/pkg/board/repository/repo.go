// Слой репозиториев - для абстракции бизнес-логики от бд
package repository

import (
	"errors"
	"fmt"
	"photosite/internal/models"
	"photosite/internal/pkg/database"
	"photosite/internal/pkg/utils/cast"
	"time"
)

const (
	insertBoard = `INSERT INTO boards(user_id, name, description, created_at, editable) 
	VALUES($1, $2, $3, $4, $5) RETURNING id;`
	selectUserBoards = `SELECT id, name, description FROM boards WHERE user_id = $1 ORDER BY created_at DESC LIMIT $2 OFFSET $3;`
	updateBoard      = `update boards set name = $1, description = $2 where id = $3 and user_id = $4 and editable = true;`
	deleteBoard      = `delete from boards where id = $1 and user_id = $2 and editable = true;`
)

type BoardRepository struct {
	db *database.DBManager
}

func NewBoardRepository(db *database.DBManager) *BoardRepository {
	return &BoardRepository{db: db}
}

func (br *BoardRepository) Create(board *models.Board, edit bool) (*models.Board, error) {
	data, err := br.db.Query(insertBoard, board.UserId, board.Name, board.Description, time.Now().Unix(), edit)
	if err != nil {
		fmt.Printf("Cannot insert board in db with user id: %d name: %s\n", board.UserId, board.Name)
		return nil, err
	}
	if len(data) == 0 {
		fmt.Printf("No id was returned by inserting board with user id: %d name: %s\n", board.UserId, board.Name)
		return nil, errors.New("Cannot create board in database")
	}
	board.Id = cast.ToInt(data[0][0])
	return board, nil
}

func (br *BoardRepository) GetUserBoards(userID, start, limit int) ([]*models.Board, error) {
	data, err := br.db.Query(selectUserBoards, userID, limit, start)
	if err != nil {
		fmt.Printf("Cannot get user boards with user id: %d start: %d limit: %d\n", userID, start, limit)
		return nil, err
	}
	if len(data) == 0 {
		fmt.Printf("Cannot get user boards with user id: %d start: %d limit: %d\n", userID, start, limit)
		return nil, errors.New("Cannot get user boards")
	}

	result := make([]*models.Board, 0)
	for _, row := range data {
		result = append(result, &models.Board{
			Id:          cast.ToInt(row[0]),
			Name:        cast.ToString(row[1]),
			Description: cast.ToString(row[2]),
			UserId:      userID,
		})
	}
	return result, nil
}

func (br *BoardRepository) Update(board *models.Board) (*models.Board, error) {
	affected, err := br.db.Exec(updateBoard, board.Name, board.Description, board.Id, board.UserId)
	if err != nil {
		fmt.Printf("Cannot update board with id: %d user id: %d\n", board.Id, board.UserId)
		return nil, err
	}
	if affected == 0 {
		fmt.Printf("Cannot update board with id: %d user id: %d\n", board.Id, board.UserId)
		return nil, errors.New("Failed to find board")
	}
	return board, nil
}

func (br *BoardRepository) Delete(id, userID int) error {
	affected, err := br.db.Exec(deleteBoard, id, userID)
	if err != nil {
		fmt.Printf("Cannot delete board with id: %d user id: %d\n", id, userID)
		return err
	}
	if affected == 0 {
		fmt.Printf("Cannot delete board with id: %d user id: %d\n", id, userID)
		return errors.New("Failed to find board")
	}
	return nil
}
