// Слой контроллеров - обработчиков http-запросов
package handler

import (
	"fmt"
	"net/http"
	"photosite/internal/models"
	"photosite/internal/pkg/board"
	"photosite/internal/pkg/utils"
	"strconv"

	"github.com/gorilla/mux"
)

type BoardHandler struct {
	boardCases board.IBoardUsecase
}

func NewBoardHandlers(router *mux.Router, uc board.IBoardUsecase) {
	handlers := BoardHandler{
		boardCases: uc,
	}

	router.HandleFunc("/boards", handlers.Create).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc("/users/{id:[0-9]+}/boards", handlers.GetUserBoards).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc("/boards/{id:[0-9]+}", handlers.Update).Methods(http.MethodPatch, http.MethodOptions)

	router.HandleFunc("/boards/{id:[0-9]+}", handlers.Delete).Methods(http.MethodDelete, http.MethodOptions)
}

func (bh *BoardHandler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	userID := r.Context().Value("id")
	if userID == 0 {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	boardForm := &models.InputBoard{}
	if utils.DecodeJSON(w, r, boardForm) != nil {
		return
	}

	createdBoard, err := bh.boardCases.Create(userID.(int), boardForm)
	if err != nil {
		http.Error(w, `{"message":"cannot create board"}`, http.StatusBadRequest)
		return
	}

	utils.EncodeJSON(w, createdBoard)
}

func (bh *BoardHandler) GetUserBoards(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find a board: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		start = 0
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		limit = 10
	}

	boards, err := bh.boardCases.GetUserBoards(id, start, limit)
	if err != nil {
		fmt.Println("Cannot get boards list")
		http.Error(w, `{"message":"Failed to find user boards"}`, http.StatusNotFound)
		return
	}

	utils.EncodeJSON(w, boards)
}

func (bh *BoardHandler) Update(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	userID := r.Context().Value("id")
	if userID == 0 {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find a board: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	boardForm := &models.InputBoard{}
	if utils.DecodeJSON(w, r, boardForm) != nil {
		return
	}

	updated, err := bh.boardCases.Update(userID.(int), id, boardForm)
	if err != nil {
		fmt.Println("Cannot update board")
		http.Error(w, `{"message":"Failed to update board"}`, http.StatusMethodNotAllowed)
		return
	}

	utils.EncodeJSON(w, updated)
}

func (bh *BoardHandler) Delete(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value("id")
	if userID == 0 {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find a board: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	err = bh.boardCases.Delete(id, userID.(int))
	if err != nil {
		fmt.Println("Cannot delete board with id", id)
		http.Error(w, `{"message":"Failed to delete board"}`, http.StatusMethodNotAllowed)
		return
	}

	w.Write([]byte(`{"message":"OK"}`))
}
