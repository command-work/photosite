// Слой юзкейсов - бизнес-логика
package usecase

import (
	"photosite/internal/models"
	"photosite/internal/pkg/board"
	"photosite/internal/pkg/pin"
)

type BoardUsecase struct {
	boardRepo board.IBoardRepository
	pinRepo   pin.IPinRepository
}

func NewBoardUsecase(br board.IBoardRepository, pr pin.IPinRepository) *BoardUsecase {
	return &BoardUsecase{
		boardRepo: br,
		pinRepo:   pr,
	}
}

func (bu *BoardUsecase) Create(userID int, form *models.InputBoard) (*models.Board, error) {
	return bu.boardRepo.Create(&models.Board{
		UserId:      userID,
		Name:        form.Name,
		Description: form.Description,
	}, true)
}

func (bu *BoardUsecase) GetUserBoards(userID, start, limit int) ([]*models.Board, error) {
	boards, err := bu.boardRepo.GetUserBoards(userID, start, limit)
	if err != nil {
		return nil, err
	}
	for i := range boards {
		pin, err := bu.pinRepo.GetBoardLastPin(boards[i].Id)
		if err != nil {
			return nil, err
		}
		boards[i].LastPin = pin
	}
	return boards, nil
}

func (bu *BoardUsecase) Update(userID, id int, form *models.InputBoard) (*models.Board, error) {
	return bu.boardRepo.Update(&models.Board{
		Id:          id,
		UserId:      userID,
		Name:        form.Name,
		Description: form.Description,
	})
}

func (bu *BoardUsecase) Delete(id, userID int) error {
	err := bu.pinRepo.DeleteBoardPins(id)
	if err != nil {
		return err
	}
	return bu.boardRepo.Delete(id, userID)
}
