package board

import (
	"photosite/internal/models"
)

type IBoardUsecase interface {
	Create(userID int, form *models.InputBoard) (*models.Board, error)
	GetUserBoards(userID, start, limit int) ([]*models.Board, error)
	Update(userID, id int, form *models.InputBoard) (*models.Board, error)
	Delete(id, userID int) error
}
