package board

import (
	"photosite/internal/models"
)

type IBoardRepository interface {
	Create(board *models.Board, edit bool) (*models.Board, error)
	GetUserBoards(userID, start, limit int) ([]*models.Board, error)
	Update(board *models.Board) (*models.Board, error)
	Delete(id, userID int) error
}
