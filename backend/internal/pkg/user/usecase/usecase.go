// Слой юзкейсов - бизнес-логика
package usecase

import (
	"fmt"
	"photosite/internal/models"
	"photosite/internal/pkg/board"
	"photosite/internal/pkg/user"

	"golang.org/x/crypto/bcrypt"
)

type UserUsecase struct {
	userRepo  user.IUserRepository
	boardRepo board.IBoardRepository
}

func NewUserUsecase(ur user.IUserRepository, br board.IBoardRepository) *UserUsecase {
	return &UserUsecase{
		userRepo:  ur,
		boardRepo: br,
	}
}

func (uu *UserUsecase) Signup(user *models.AuthUser) (*models.User, error) {
	password, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	newUser := &models.User{
		Email:    user.Email,
		Login:    user.Login,
		Password: password,
	}
	newUser.ID, err = uu.userRepo.Create(newUser)
	if err != nil {
		return nil, err
	}

	defaultBoard := &models.Board{
		UserId:      newUser.ID,
		Name:        "Моя доска",
		Description: "Пины лучшего пользователя на свете",
	}
	if _, err = uu.boardRepo.Create(defaultBoard, false); err != nil {
		return nil, err
	}
	return newUser, nil
}

func (uu *UserUsecase) Login(user *models.AuthUser) (*models.User, error) {
	loginUser, err := uu.userRepo.GetByLogin(user.Login)
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword(loginUser.Password, []byte(user.Password))
	if err != nil {
		fmt.Println("Passwords do not match")
		return nil, err
	}
	return loginUser, nil
}

func (uu *UserUsecase) GetUserById(id, target int) (*models.User, error) {
	user, err := uu.userRepo.GetById(id)
	if err != nil {
		return nil, err
	}
	if target != 0 && uu.userRepo.CheckSubscription(target, id) {
		user.IsSub = 1
	}
	return user, nil
}

func (uu *UserUsecase) FindUsersByLogin(login string) ([]*models.User, error) {
	foundUser, err := uu.userRepo.GetByLogin(login)
	return []*models.User{foundUser}, err
}

func (uu *UserUsecase) CreateSub(userID, targetID int) error {
	err := uu.userRepo.CreateSub(userID, targetID)
	if err != nil {
		return err
	}
	err = uu.userRepo.UpdateSubCount(userID, "+1")
	if err != nil {
		return err
	}
	return uu.userRepo.UpdateSubscriberCount(targetID, "+1")
}

func (uu *UserUsecase) DeleteSub(userID, targetID int) error {
	err := uu.userRepo.DeleteSub(userID, targetID)
	if err != nil {
		return err
	}
	err = uu.userRepo.UpdateSubCount(userID, "-1")
	if err != nil {
		return err
	}
	return uu.userRepo.UpdateSubscriberCount(targetID, "-1")
}

func (uu *UserUsecase) GetSubscriptions(userID, start, limit int) ([]*models.User, error) {
	return uu.userRepo.GetSubscriptions(userID, start, limit)
}

func (uu *UserUsecase) GetSubscribers(userID, start, limit int) ([]*models.User, error) {
	return uu.userRepo.GetSubscribers(userID, start, limit)
}
