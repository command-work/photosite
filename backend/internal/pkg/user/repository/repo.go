// Слой репозиториев - для абстракции бизнес-логики от бд
package repository

import (
	"errors"
	"fmt"
	"photosite/internal/models"
	"photosite/internal/pkg/database"
	"photosite/internal/pkg/utils/cast"
	"time"
)

const (
	insertUser = `INSERT INTO users(email, login, password, about, avatar, 
	subscriptions, subscribers, created_at) 
	VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;`

	selectLoginUser = `SELECT id, email, password, avatar, about, subscriptions, 
	subscribers FROM users WHERE login = $1;`

	selectUser = "SELECT email, login, password, avatar, about, subscriptions, subscribers FROM users " +
		"WHERE id = $1;"
	insertSub             = `insert into subscriptions values(default, $1, $2);`
	deleteSub             = `delete from subscriptions where user_id = $1 and subscribed_at = $2;`
	updateSubCount        = `update users set subscriptions = subscriptions%s where id = $1;`
	updateSubscriberCount = `update users set subscribers = subscribers%s where id = $1;`
	selectSubscriptions   = `select u.id, u.login, u.avatar, u.about, u.subscriptions, u.subscribers 
	from users as u join subscriptions as s on u.id = s.subscribed_at where s.user_id = $1 limit $2 offset $3;`
	selectSubscribers = `select u.id, u.login, u.avatar, u.about, u.subscriptions, u.subscribers 
	from users as u join subscriptions as s on u.id = s.user_id where s.subscribed_at = $1 limit $2 offset $3;`
	selectSubID = `select id from subscriptions where user_id = $1 and subscribed_at = $2;`
)

type UserRepository struct {
	db *database.DBManager
}

func NewUserRepository(db *database.DBManager) *UserRepository {
	return &UserRepository{db: db}
}

func (ur *UserRepository) Create(user *models.User) (int, error) {
	data, err := ur.db.Query(insertUser, user.Email, user.Login, user.Password,
		user.About, user.Avatar, user.Subscriptions, user.Subscribers, time.Now().Unix())
	if err != nil {
		fmt.Printf("Cannot insert user in db with login: %s email: %s\n", user.Login, user.Email)
		return 0, err
	}
	if len(data) == 0 {
		fmt.Printf("No id was returned by inserting user with login: %s email: %s\n", user.Login, user.Email)
		return 0, errors.New("Cannot create user in database")
	}
	return cast.ToInt(data[0][0]), nil
}

func (ur *UserRepository) GetByLogin(login string) (*models.User, error) {
	data, err := ur.db.Query(selectLoginUser, login)
	if err != nil {
		fmt.Printf("Cannot find user in db with login: %s\n", login)
		return nil, err
	}
	if len(data) == 0 {
		fmt.Printf("Cannot find user in db with login: %s\n", login)
		return nil, errors.New("Failed to find a user with the given ID")
	}
	user := &models.User{
		ID:            cast.ToInt(data[0][0]),
		Email:         cast.ToString(data[0][1]),
		Password:      data[0][2],
		Login:         login,
		Avatar:        cast.ToString(data[0][3]),
		About:         cast.ToString(data[0][4]),
		Subscriptions: cast.ToInt(data[0][5]),
		Subscribers:   cast.ToInt(data[0][6]),
	}
	return user, nil
}

func (ur *UserRepository) GetById(id int) (*models.User, error) {
	data, err := ur.db.Query(selectUser, id)
	if err != nil {
		fmt.Printf("An error occured while requesting a user with the given ID: %d\n", id)
		return nil, err
	}
	if len(data) == 0 {
		fmt.Printf("Failed to find a user with the given ID: %d\n", id)
		return nil, errors.New("Failed to find a user with the given ID")
	}
	user := &models.User{
		ID:            id,
		Email:         cast.ToString(data[0][0]),
		Login:         cast.ToString(data[0][1]),
		Password:      data[0][2],
		Avatar:        cast.ToString(data[0][3]),
		About:         cast.ToString(data[0][4]),
		Subscriptions: cast.ToInt(data[0][5]),
		Subscribers:   cast.ToInt(data[0][6]),
	}
	return user, nil
}

func (ur *UserRepository) CreateSub(userID, targetID int) error {
	affected, err := ur.db.Exec(insertSub, userID, targetID)
	if err != nil {
		fmt.Printf("Cannot create sub with id: %d target id: %d\n", userID, targetID)
		return err
	}
	if affected == 0 {
		fmt.Printf("Cannot create sub with id: %d target id: %d\n", userID, targetID)
		return errors.New("Failed to create sub")
	}
	return nil
}

func (ur *UserRepository) DeleteSub(userID, targetID int) error {
	affected, err := ur.db.Exec(deleteSub, userID, targetID)
	if err != nil {
		fmt.Printf("Cannot delete sub with id: %d target id: %d\n", userID, targetID)
		return err
	}
	if affected == 0 {
		fmt.Printf("Cannot delete sub with id: %d target id: %d\n", userID, targetID)
		return errors.New("Failed to delete sub")
	}
	return nil
}

func (ur *UserRepository) UpdateSubCount(userID int, operation string) error {
	query := fmt.Sprintf(updateSubCount, operation)
	affected, err := ur.db.Exec(query, userID)
	if err != nil {
		fmt.Println("Cannot update sub count with user id: ", userID)
		return err
	}
	if affected == 0 {
		fmt.Println("Cannot update sub count with user id: ", userID)
		return errors.New("Failed to update sub count")
	}
	return nil
}

func (ur *UserRepository) UpdateSubscriberCount(userID int, operation string) error {
	query := fmt.Sprintf(updateSubscriberCount, operation)
	affected, err := ur.db.Exec(query, userID)
	if err != nil {
		fmt.Println("Cannot update subscriber count with user id: ", userID)
		return err
	}
	if affected == 0 {
		fmt.Println("Cannot update subscriber count with user id: ", userID)
		return errors.New("Failed to update subscriber count")
	}
	return nil
}

func (ur *UserRepository) GetSubscriptions(userID, start, limit int) ([]*models.User, error) {
	data, err := ur.db.Query(selectSubscriptions, userID, limit, start)
	if err != nil {
		fmt.Println("Cannot find subscriptions in db for user: ", userID)
		return nil, err
	}

	result := make([]*models.User, 0)
	for _, row := range data {
		result = append(result, &models.User{
			ID:            cast.ToInt(row[0]),
			Login:         cast.ToString(row[1]),
			Avatar:        cast.ToString(row[2]),
			About:         cast.ToString(row[3]),
			Subscriptions: cast.ToInt(row[4]),
			Subscribers:   cast.ToInt(row[5]),
		})
	}
	return result, nil
}

func (ur *UserRepository) GetSubscribers(userID, start, limit int) ([]*models.User, error) {
	data, err := ur.db.Query(selectSubscribers, userID, limit, start)
	if err != nil {
		fmt.Println("Cannot find subscribers in db for user: ", userID)
		return nil, err
	}

	result := make([]*models.User, 0)
	for _, row := range data {
		result = append(result, &models.User{
			ID:            cast.ToInt(row[0]),
			Login:         cast.ToString(row[1]),
			Avatar:        cast.ToString(row[2]),
			About:         cast.ToString(row[3]),
			Subscriptions: cast.ToInt(row[4]),
			Subscribers:   cast.ToInt(row[5]),
		})
	}
	return result, nil
}

func (ur *UserRepository) CheckSubscription(userID, targetID int) bool {
	data, err := ur.db.Query(selectSubID, userID, targetID)
	if err != nil || len(data) == 0 {
		return false
	}
	return true
}
