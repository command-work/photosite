package user

import (
	"photosite/internal/models"
)

type IUserUsecase interface {
	Signup(user *models.AuthUser) (*models.User, error)
	Login(user *models.AuthUser) (*models.User, error)
	GetUserById(id, target int) (*models.User, error)
	FindUsersByLogin(login string) ([]*models.User, error)
	CreateSub(userID, targetID int) error
	DeleteSub(userID, targetID int) error
	GetSubscriptions(userID, start, limit int) ([]*models.User, error)
	GetSubscribers(userID, start, limit int) ([]*models.User, error)
}
