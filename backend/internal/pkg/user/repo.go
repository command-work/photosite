package user

import (
	"photosite/internal/models"
)

type IUserRepository interface {
	Create(user *models.User) (int, error)
	GetByLogin(login string) (*models.User, error)
	GetById(id int) (*models.User, error)
	CreateSub(userID, targetID int) error
	DeleteSub(userID, targetID int) error
	UpdateSubCount(userID int, operation string) error
	UpdateSubscriberCount(userID int, operation string) error
	GetSubscriptions(userID, start, limit int) ([]*models.User, error)
	GetSubscribers(userID, start, limit int) ([]*models.User, error)
	CheckSubscription(userID, targetID int) bool
}
