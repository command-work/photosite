// Слой контроллеров - обработчиков http-запросов
package handler

import (
	"fmt"
	"io"
	"net/http"
	"photosite/internal/models"
	"photosite/internal/pkg/user"
	"photosite/internal/pkg/utils"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/robbert229/jwt"
)

type UserHandler struct {
	userCases user.IUserUsecase
}

func NewUserHandlers(router *mux.Router, uc user.IUserUsecase) {
	handlers := UserHandler{
		userCases: uc,
	}

	router.HandleFunc("/users", handlers.Signup).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc("/auth", handlers.Login).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc("/auth", func(w http.ResponseWriter, r *http.Request) {
		utils.EncodeJSON(w, r.Context().Value("id"))
	}).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc("/users/{id:[0-9]+}", handlers.GetUserById).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc("/users/{id:[0-9]+}/subscriptions", handlers.Subscription).Methods(http.MethodPost, http.MethodDelete, http.MethodOptions)

	router.HandleFunc("/users/{id:[0-9]+}/subscriptions", handlers.GetSubscriptions).Methods(http.MethodGet, http.MethodOptions)

	router.HandleFunc("/users/{id:[0-9]+}/subscribers", handlers.GetSubscribers).Methods(http.MethodGet, http.MethodOptions)
}

func (uh *UserHandler) Signup(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	signupForm := &models.AuthUser{}
	if utils.DecodeJSON(w, r, signupForm) != nil {
		return
	}

	createdUser, err := uh.userCases.Signup(signupForm)
	if err != nil {
		http.Error(w, `{"message":"cannot signup"}`, http.StatusBadRequest)
		return
	}

	algorithm := jwt.HmacSha256(models.JWTKey)
	claims := jwt.NewClaim()
	claims.Set("Identificator", createdUser.ID)

	token, err := algorithm.Encode(claims)
	if err != nil {
		fmt.Println(err)
		http.Error(w, `{"message":"cannot create jwt-token"}`, http.StatusInternalServerError)
		return
	}
	createdUser.TokenJWT = token

	utils.EncodeJSON(w, createdUser)
}

func (uh *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	loginForm := &models.AuthUser{}
	if utils.DecodeJSON(w, r, loginForm) != nil {
		return
	}

	loginUser, err := uh.userCases.Login(loginForm)
	if err != nil {
		http.Error(w, `{"message":"cannot login"}`, http.StatusNotFound)
		return
	}

	algorithm := jwt.HmacSha256(models.JWTKey)
	claims := jwt.NewClaim()
	claims.Set("Identificator", loginUser.ID)

	token, err := algorithm.Encode(claims)
	if err != nil {
		fmt.Println(err)
		http.Error(w, `{"message":"cannot create jwt-token"}`, http.StatusInternalServerError)
		return
	}
	loginUser.TokenJWT = token

	utils.EncodeJSON(w, loginUser)
}

func (uh *UserHandler) GetUserById(w http.ResponseWriter, r *http.Request) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			// ToDo: log error
		}
	}(r.Body)
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		// Should never occur since a router has a built-in regexp that only passes integers on this handler
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to find a user: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	userID := r.Context().Value("id")
	foundUser, err := uh.userCases.GetUserById(id, userID.(int))
	err = utils.EncodeJSON(w, foundUser)
	if err != nil {
		return
	}
}

func (uh *UserHandler) Subscription(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find user: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	target, err := strconv.Atoi(r.URL.Query().Get("target_id"))
	if err != nil {
		http.Error(w, `{"message":"Failed to find user: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	userID := r.Context().Value("id")
	if userID == 0 || id != userID {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	switch r.Method {
	case http.MethodPost:
		err = uh.userCases.CreateSub(userID.(int), target)
	case http.MethodDelete:
		err = uh.userCases.DeleteSub(userID.(int), target)
	default:
		http.Error(w, `{"message":"Unknown method for subscriptions"}`, http.StatusMethodNotAllowed)
		return
	}
	if err != nil {
		http.Error(w, `{"message":"Failed to find user for subscription"}`, http.StatusNotFound)
	}

	w.Write([]byte(`{"message":"OK"}`))
}

func (uh *UserHandler) GetSubscriptions(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find user: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		start = 0
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		limit = 10
	}

	users, err := uh.userCases.GetSubscriptions(id, start, limit)
	if err != nil {
		fmt.Println("Cannot find subscriptions for user id: ", id)
		http.Error(w, `{"message":"Failed to find user for subscriptions"}`, http.StatusNotFound)
		return
	}

	utils.EncodeJSON(w, users)
}

func (uh *UserHandler) GetSubscribers(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find user: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		start = 0
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		limit = 10
	}

	users, err := uh.userCases.GetSubscribers(id, start, limit)
	if err != nil {
		fmt.Println("Cannot find subscribers for user id: ", id)
		http.Error(w, `{"message":"Failed to find user for subscribers"}`, http.StatusNotFound)
		return
	}

	utils.EncodeJSON(w, users)
}
