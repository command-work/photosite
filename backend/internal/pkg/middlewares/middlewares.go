package middlewares

import (
	"context"
	"fmt"
	"net/http"
	"photosite/internal/models"
	"photosite/internal/pkg/utils"
	"strings"
	"time"

	"github.com/robbert229/jwt"
)

type GoMiddleware struct {
}

func InitMiddleware() *GoMiddleware {
	return &GoMiddleware{}
}

func (m *GoMiddleware) PanicRecoverMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("Recovered from panic with err: '%s' on url: %s\n", err, r.RequestURI)
				http.Error(w, `{"message":"server"}`, http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func (m *GoMiddleware) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		fmt.Printf("%s %s %s %s\n", r.Method, r.RequestURI, r.RemoteAddr, time.Since(start))
	})
}

func (m *GoMiddleware) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		var id int = 0

		authHeader := r.Header.Get("Authorization")
		if authHeader != "" {
			authToken := strings.Split(authHeader, " ")[1]
			algorithm := jwt.HmacSha256(models.JWTKey)

			if claims, err := algorithm.DecodeAndValidate(authToken); err == nil {
				if identificator, err := claims.Get("Identificator"); err == nil {
					id = int(identificator.(float64))
				}
			}
		}

		ctx = context.WithValue(ctx, "id", id)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func (m *GoMiddleware) CORSMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		origin := r.Header.Get("Origin")
		if origin == "" {
			origin = "*"
		}

		w.Header().Set("Access-Control-Allow-Methods", "POST,PUT,DELETE,GET,PATCH")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token,Authorization")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Origin", origin)
		if r.Method == http.MethodOptions {
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (m *GoMiddleware) CSRFMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			csrf := r.Header.Get("X-CSRF-Token")
			if csrf == "" {
				http.SetCookie(w, &http.Cookie{
					Name:    "csrf_token",
					Value:   utils.RandString(32),
					Path:    "/",
					Expires: time.Now().Add(time.Minute * 200),
				})
				http.Error(w, `{"message":"no csrf-token found"}`, http.StatusMethodNotAllowed)
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}
