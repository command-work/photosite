// Вспомогательные функции
package utils

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func DecodeJSON(w http.ResponseWriter, r *http.Request, v interface{}) error {
	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		fmt.Println("Decoding json error: ", err)
		http.Error(w, `{"message":"invalid json to decode"}`, http.StatusBadRequest)
	}
	return err
}

func EncodeJSON(w http.ResponseWriter, v interface{}) error {
	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		fmt.Println("Encoding json error: ", err)
		http.Error(w, `{"message":"cannot encode data to json"}`, http.StatusInternalServerError)
	}
	return err
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}
