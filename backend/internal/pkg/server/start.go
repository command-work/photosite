package server

import (
	"net/http"
	"os"
	"os/signal"
	boardHandler "photosite/internal/pkg/board/handler"
	boardRepository "photosite/internal/pkg/board/repository"
	boardUsecase "photosite/internal/pkg/board/usecase"
	commentHandler "photosite/internal/pkg/comment/handler"
	commentRepository "photosite/internal/pkg/comment/repository"
	commentUsecase "photosite/internal/pkg/comment/usecase"
	"photosite/internal/pkg/database"
	"photosite/internal/pkg/middlewares"
	pinHandler "photosite/internal/pkg/pin/handler"
	pinRepository "photosite/internal/pkg/pin/repository"
	pinUsecase "photosite/internal/pkg/pin/usecase"
	searchHandler "photosite/internal/pkg/search/handler"
	userHandler "photosite/internal/pkg/user/handler"
	userRepository "photosite/internal/pkg/user/repository"
	userUsecase "photosite/internal/pkg/user/usecase"
	"syscall"

	"github.com/gorilla/mux"
)

func RunServer(addr string, flag bool) error {
	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)

	apiRouter := r.PathPrefix("/api").Subrouter()

	mid := middlewares.InitMiddleware()
	r.Use(mid.PanicRecoverMiddleware)
	apiRouter.Use(mid.CORSMiddleware)
	apiRouter.Use(mid.CSRFMiddleware)
	apiRouter.Use(mid.LoggingMiddleware)
	apiRouter.Use(mid.AuthMiddleware)

	dbHost := "localhost"
	if flag {
		dbHost = "database"
	}
	db := database.Connect(dbHost)

	userRepo := userRepository.NewUserRepository(db)
	pinRepo := pinRepository.NewPinRepository(db)
	boardRepo := boardRepository.NewBoardRepository(db)
	commentRepo := commentRepository.NewCommentRepository(db)

	userUC := userUsecase.NewUserUsecase(userRepo, boardRepo)
	pinUC := pinUsecase.NewPinUsecase(pinRepo, userRepo)
	commentUC := commentUsecase.NewCommentUsecase(commentRepo, userRepo)
	boardUC := boardUsecase.NewBoardUsecase(boardRepo, pinRepo)

	userHandler.NewUserHandlers(apiRouter, userUC)
	pinHandler.NewPinHandlers(apiRouter, pinUC)
	commentHandler.NewCommentHandlers(apiRouter, commentUC)
	boardHandler.NewBoardHandlers(apiRouter, boardUC)
	searchHandler.NewSearchHandlers(apiRouter, userUC, pinUC, commentUC)

	server := http.Server{
		Addr:    addr,
		Handler: r,
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		database.Disconnect(db)
		os.Exit(0)
	}()

	return server.ListenAndServe()
}
