// Здесь будет описано подключение к базе и обращение к ней
package database

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PgxPool interface {
	Begin(context.Context) (pgx.Tx, error)
	Close()
}

type DBManager struct {
	Pool PgxPool
}

func Connect(host string) *DBManager {
	connString := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable", "totw", "photosite", host, 5432, "photosite")
	pool, err := pgxpool.Connect(context.Background(), connString)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil
	}
	fmt.Println("Successful connection to postgres")
	return &DBManager{Pool: pool}
}

func Disconnect(manager *DBManager) {
	manager.Pool.Close()
	fmt.Println("DB was disconnected")
}

func (db *DBManager) Query(queryString string, params ...interface{}) ([][][]byte, error) {
	ctx := context.Background()
	tx, err := db.Pool.Begin(ctx)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil, err
	}
	defer tx.Rollback(ctx)

	rows, err := tx.Query(ctx, queryString, params...)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil, err
	}
	defer rows.Close()

	result := make([][][]byte, 0)
	for rows.Next() {
		row := make([][]byte, 0)
		row = append(row, rows.RawValues()...)
		result = append(result, row)
	}

	err = tx.Commit(ctx)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil, err
	}
	return result, nil
}

func (db *DBManager) Exec(queryString string, params ...interface{}) (int, error) {
	ctx := context.Background()
	tx, err := db.Pool.Begin(ctx)
	if err != nil {
		fmt.Println("Error: ", err)
		return 0, err
	}
	defer tx.Rollback(ctx)

	result, err := tx.Exec(ctx, queryString, params...)
	if err != nil {
		fmt.Println("Error: ", err)
		return 0, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		fmt.Println("Error: ", err)
		return 0, err
	}

	if result.RowsAffected() == 0 {
		fmt.Println("No row was changed")
	}
	return int(result.RowsAffected()), nil
}
