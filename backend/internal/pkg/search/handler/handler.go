package handler

import (
	"github.com/gorilla/mux"
	"net/http"
	"photosite/internal/pkg/comment"
	"photosite/internal/pkg/pin"
	"photosite/internal/pkg/user"
	"photosite/internal/pkg/utils"
	"strconv"
)

type SearchHandler struct {
	commentCases comment.ICommentUsecase
	pinCases pin.IPinUsecase
	userCases user.IUserUsecase
}

func NewSearchHandlers(router *mux.Router, uc user.IUserUsecase, pc pin.IPinUsecase, cc comment.ICommentUsecase) {
	handlers := SearchHandler{
		commentCases: cc,
		pinCases: pc,
		userCases: uc,
	}

	router.HandleFunc("/search", handlers.Search).Methods(http.MethodGet, http.MethodOptions)
}

func (sh *SearchHandler) Search(w http.ResponseWriter, r *http.Request) {
	what := r.URL.Query().Get("what")
	description := r.URL.Query().Get("description")
	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		http.Error(w, `{"message":"Failed to get comments: wrong query parameter: start"}`, http.StatusBadRequest)
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		http.Error(w, `{"message":"Failed to get comments: wrong query parameter: limit"}`, http.StatusBadRequest)
	}
	switch what {
	case "comment":
		comments, err := sh.commentCases.GetByText(description, start, limit)
		if err != nil {
			http.Error(w, `{"message":"Failed to find matching comments in storage"}`, http.StatusNotFound)
			return
		}

		if utils.EncodeJSON(w, comments) != nil {
			http.Error(w, `{"message":" Failed on encoding result"}`, http.StatusInternalServerError)
		}
	case "pin":
		date := r.URL.Query().Get("date")
		if date != "day" && date != "week" && date != "month" && date != "" {
			http.Error(w, `{"message":"Failed to search for pins: wrong query parameter: date"}`, http.StatusBadRequest)
		}

		var desc bool
		descString := r.URL.Query().Get("desc")
		if descString == "true" {
			desc = true
		} else if descString == "false" {
			desc = false
		} else {
			http.Error(w, `{"message":"Failed to search for pins: wrong query parameter: desc"}`, http.StatusBadRequest)
			return
		}

		most := r.URL.Query().Get("most")
		if most != "popular" && most != "commented" && most != "" {
			http.Error(w, `{"message":"Failed to search for pins: wrong query parameter: most"}`, http.StatusBadRequest)
			return
		}

		pins, err := sh.pinCases.GetByName(description, start, limit, date, desc, most)
		if err != nil {
			http.Error(w, `{"message":"Failed to find matching pins in storage"}`, http.StatusNotFound)
			return
		}

		if utils.EncodeJSON(w, pins) != nil {
			http.Error(w, `{"message":" Failed on encoding result"}`, http.StatusInternalServerError)
		}
	case "user":
		users, err := sh.userCases.FindUsersByLogin(description)
		if err != nil {
			http.Error(w, `{"message":"Failed to find matching users in storage"}`, http.StatusNotFound)
			return
		}

		if utils.EncodeJSON(w, users) != nil {
			http.Error(w, `{"message":" Failed on encoding result"}`, http.StatusInternalServerError)
		}
	default:
		http.Error(w, `{"message":"Searching failed: wrong query parameter: what"}`, http.StatusBadRequest)
	}
}