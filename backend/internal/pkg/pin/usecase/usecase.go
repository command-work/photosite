// Слой юзкейсов - бизнес-логика
package usecase

import (
	"photosite/internal/models"
	"photosite/internal/pkg/pin"
	"photosite/internal/pkg/user"
)

type PinUsecase struct {
	pinRepo  pin.IPinRepository
	userRepo user.IUserRepository
}

func NewPinUsecase(pr pin.IPinRepository, ur user.IUserRepository) *PinUsecase {
	return &PinUsecase{
		pinRepo:  pr,
		userRepo: ur,
	}
}

func (pu *PinUsecase) AddPin(pin *models.Pin) (int, error) {
	id, err := pu.pinRepo.Create(pin)
	return id, err
}

func (pu *PinUsecase) GetById(id int) (*models.PinWithUser, error) {
	pu.pinRepo.UpdateViews(id)
	foundPin, err := pu.pinRepo.GetById(id)
	if err == nil {
		foundUser, err := pu.userRepo.GetById(foundPin.UserID)
		if err == nil {
			res := &models.PinWithUser{
				ID:          foundPin.ID,
				FullUser:    foundUser,
				BoardID:     foundPin.BoardID,
				Name:        foundPin.Name,
				Image:       foundPin.Image,
				Description: foundPin.Description,
				CreatedAt:   foundPin.CreatedAt,
				Tags:        foundPin.Tags,
				Views:       foundPin.Views,
				Comments:    foundPin.Comments,
			}
			return res, nil
		}
	}
	return nil, err
}

func (pu *PinUsecase) GetMainPins(start, limit int) ([]*models.Pin, error) {
	return pu.pinRepo.GetMainPins(start, limit)
}

func (pu *PinUsecase) GetSubPins(userID, start, limit int) ([]*models.Pin, error) {
	return pu.pinRepo.GetSubPins(userID, start, limit)
}

func (pu *PinUsecase) GetBoardPins(boardID, start, limit int) ([]*models.Pin, error) {
	return pu.pinRepo.GetBoardPins(boardID, start, limit)
}

func (pu *PinUsecase) GetUserPins(userID, start, limit int) ([]*models.Pin, error) {
	return pu.pinRepo.GetUserPins(userID, start, limit)
}

func (pu *PinUsecase) UpdatePin(id, userID int, pin *models.Pin) error {
	if err := pu.pinRepo.Update(id, userID, pin); err != nil {
		return err
	}
	pin.ID = id
	pin.UserID = userID
	return nil
}

func (pu *PinUsecase) DeletePin(id, userID int) error {
	return pu.pinRepo.Delete(id, userID)
}

func (pu *PinUsecase) SavePin(id, boardID int) error {
	return pu.pinRepo.SaveToBoard(id, boardID)
}

func (pu *PinUsecase) GetByName(name string, start int, limit int, date string, desc bool, most string) ([]*models.Pin, error) {
	return pu.pinRepo.GetByName(name, start, limit, date, desc, most)
}
