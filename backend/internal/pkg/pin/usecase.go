package pin

import (
	"photosite/internal/models"
)

type IPinUsecase interface {
	AddPin(pin *models.Pin) (int, error)
	GetById(id int) (*models.PinWithUser, error)
	GetMainPins(start, limit int) ([]*models.Pin, error)
	GetSubPins(userID, start, limit int) ([]*models.Pin, error)
	GetBoardPins(boardID, start, limit int) ([]*models.Pin, error)
	GetUserPins(userID, start, limit int) ([]*models.Pin, error)
	UpdatePin(id, userID int, pin *models.Pin) error
	DeletePin(id, userID int) error
	SavePin(id, boardID int) error
	GetByName(name string, start int, limit int, date string, desc bool, most string) ([]*models.Pin, error)
}
