// Слой репозиториев - для абстракции бизнес-логики от бд
package repository

import (
	"errors"
	"fmt"
	"photosite/internal/models"
	"photosite/internal/pkg/database"
	"photosite/internal/pkg/utils/cast"
	"time"
)

const (
	insertPin = "INSERT INTO pins(user_id, name, image, description, created_at, views, comments) " +
		"VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING id;"
	selectPin       = `SELECT id, user_id, name, description, image, created_at, views, comments FROM pins where id=$1;`
	updatePin       = `update pins set name = $1, description = $2 where id = $3 and user_id = $4;`
	deletePin       = `delete from pins where id = $1 and user_id = $2;`
	deleteBoardPins = `delete from pins where id in (select pin_id from boards_pins where board_id = $1 and original = true);`

	selectPinTags = "select tag from tags where id in (" +
		"select tag_id from pins_tags " +
		"where pin_id = $1);"
	insertUniqueTag = "select add_tag($1, $2);"

	selectLastPin = `select id, name, description, user_id, image
	from pins as p join boards_pins as bp on p.id = bp.pin_id
	where bp.board_id = $1 order by p.created_at desc limit 1;`

	insertOnBoard = "insert into boards_pins(pin_id, board_id, original) values($1, $2, $3);"

	selectMainPins = `SELECT p.id, p.user_id, p.name, p.description, p.image, p.created_at, bp.board_id
	FROM pins as p JOIN boards_pins as bp ON p.id = bp.pin_id 
	WHERE bp.original = true ORDER BY p.views DESC LIMIT $1 OFFSET $2;`
	selectSubPins = `SELECT p.id, p.user_id, p.name, p.description, p.image, p.created_at, bp.board_id
	FROM pins as p JOIN boards_pins as bp ON p.id = bp.pin_id 
	JOIN subscriptions as s ON s.subscribed_at = p.user_id
	WHERE s.user_id = $1 AND bp.original = true ORDER BY p.created_at DESC LIMIT $2 OFFSET $3;`
	selectBoardPins = `SELECT p.id, p.user_id, p.name, p.description, p.image, p.created_at, bp.board_id
	FROM pins as p JOIN boards_pins as bp ON p.id = bp.pin_id 
	WHERE bp.board_id = $1 AND bp.original = true ORDER BY p.created_at DESC LIMIT $2 OFFSET $3;`
	selectUserPins = `SELECT p.id, p.user_id, p.name, p.description, p.image, p.created_at, bp.board_id
	FROM pins as p JOIN boards_pins as bp ON p.id = bp.pin_id 
	WHERE p.user_id = $1 AND bp.original = true ORDER BY p.created_at DESC LIMIT $2 OFFSET $3;`

	selectPinsByName = "SELECT p.id, p.user_id, p.name, p.description, p.image, p.created_at, p.views, p.comments, bp.board_id FROM pins as p " +
		"JOIN boards_pins as bp ON p.id = bp.pin_id " +
		"WHERE name like $1 and created_at > $2 and created_at < $3 and bp.original = true " +
		"ORDER BY $4 %s " +
		"OFFSET $5 LIMIT $6;"
	updateViews = `update pins set views = views + 1 where id = $1;`
)

type PinRepository struct {
	db *database.DBManager
}

func NewPinRepository(db *database.DBManager) *PinRepository {
	return &PinRepository{db: db}
}

func (ur *PinRepository) Create(pin *models.Pin) (int, error) {
	data, err := ur.db.Query(insertPin, pin.UserID, pin.Name, pin.Image, pin.Description,
		time.Now().Unix(), 0, 0)
	if err != nil {
		fmt.Printf("Failed to insert a pin named %s by user with id %d in db\n", pin.Name, pin.UserID)
		// ToDo: better log it
		return 0, err
	}
	if len(data) == 0 {
		fmt.Printf("No id was returned by inserting a pin named %s by user with id %d in db\n",
			pin.Name, pin.UserID)
		return 0, errors.New("Cannot create pin in database")
	}

	id := cast.ToInt(data[0][0])
	_, err = ur.db.Exec(insertOnBoard, id, pin.BoardID, true)
	for i := 0; i < len(pin.Tags) && err == nil; i++ {
		data, err = ur.db.Query(insertUniqueTag, id, pin.Tags[i])
	}

	if err != nil {
		return 0, err
	}
	return id, nil
}

func (ur *PinRepository) SaveToBoard(id, boardID int) error {
	_, err := ur.db.Exec(insertOnBoard, id, boardID, false)
	if err != nil {
		fmt.Printf("Cannot dave pin id: %d to board id: %d\n", id, boardID)
		return err
	}
	return nil
}

func (ur *PinRepository) GetById(id int) (*models.Pin, error) {
	data, err := ur.db.Query(selectPin, id)
	if err != nil {
		fmt.Printf("Failed to get a pin with id=%d from db\n", id)
	}
	var res *models.Pin
	switch len(data) {
	case 0:
		err = errors.New("Failed to find a pin with the given ID")
		res = nil
	case 1:
		res = &models.Pin{
			ID:          cast.ToInt(data[0][0]),
			UserID:      cast.ToInt(data[0][1]),
			Name:        cast.ToString(data[0][2]),
			Description: cast.ToString(data[0][3]),
			Image:       cast.ToString(data[0][4]),
			CreatedAt:   time.Unix(int64(cast.ToInt(data[0][5])), 0),
			Views:       cast.ToInt(data[0][6]),
			Comments:    cast.ToInt(data[0][7]),
		}
		data, err = ur.db.Query(selectPinTags, id)
		if err == nil {
			for _, row := range data {
				res.Tags = append(res.Tags, cast.ToString(row[0]))
			}
		} else {
			res = nil
		}
	default:
		err = errors.New("Database failure: not unique ID") // This should never occur
		res = nil
	}
	return res, err
}

func (pr *PinRepository) GetBoardLastPin(boardID int) (*models.Pin, error) {
	data, err := pr.db.Query(selectLastPin, boardID)
	if err != nil {
		fmt.Printf("Cannot get last pin with board id: %d\n", boardID)
		return nil, err
	}
	if len(data) == 0 {
		return nil, nil
	}
	return &models.Pin{
		ID:          cast.ToInt(data[0][0]),
		Name:        cast.ToString(data[0][1]),
		Description: cast.ToString(data[0][2]),
		UserID:      cast.ToInt(data[0][3]),
		Image:       cast.ToString(data[0][4]),
		BoardID:     boardID,
	}, nil
}

func (pr *PinRepository) GetMainPins(start, limit int) ([]*models.Pin, error) {
	data, err := pr.db.Query(selectMainPins, limit, start)
	if err != nil {
		fmt.Println("Cannot get main pins from db")
		return nil, err
	}

	result := make([]*models.Pin, 0)
	for _, row := range data {
		pin := &models.Pin{
			ID:          cast.ToInt(row[0]),
			UserID:      cast.ToInt(row[1]),
			Name:        cast.ToString(row[2]),
			Description: cast.ToString(row[3]),
			Image:       cast.ToString(row[4]),
			CreatedAt:   time.Unix(int64(cast.ToInt(row[5])), 0),
			BoardID:     cast.ToInt(row[6]),
		}
		result = append(result, pin)
	}
	return result, nil
}

func (pr *PinRepository) GetSubPins(userID, start, limit int) ([]*models.Pin, error) {
	data, err := pr.db.Query(selectSubPins, userID, limit, start)
	if err != nil {
		fmt.Println("Cannot get main pins from db")
		return nil, err
	}

	result := make([]*models.Pin, 0)
	for _, row := range data {
		pin := &models.Pin{
			ID:          cast.ToInt(row[0]),
			UserID:      cast.ToInt(row[1]),
			Name:        cast.ToString(row[2]),
			Description: cast.ToString(row[3]),
			Image:       cast.ToString(row[4]),
			CreatedAt:   time.Unix(int64(cast.ToInt(row[5])), 0),
			BoardID:     cast.ToInt(row[6]),
		}
		result = append(result, pin)
	}
	return result, nil
}

func (pr *PinRepository) GetBoardPins(boardID, start, limit int) ([]*models.Pin, error) {
	data, err := pr.db.Query(selectBoardPins, boardID, limit, start)
	if err != nil {
		fmt.Println("Cannot get main pins from db")
		return nil, err
	}

	result := make([]*models.Pin, 0)
	for _, row := range data {
		pin := &models.Pin{
			ID:          cast.ToInt(row[0]),
			UserID:      cast.ToInt(row[1]),
			Name:        cast.ToString(row[2]),
			Description: cast.ToString(row[3]),
			Image:       cast.ToString(row[4]),
			CreatedAt:   time.Unix(int64(cast.ToInt(row[5])), 0),
			BoardID:     cast.ToInt(row[6]),
		}
		result = append(result, pin)
	}
	return result, nil
}

func (pr *PinRepository) GetUserPins(userID, start, limit int) ([]*models.Pin, error) {
	data, err := pr.db.Query(selectUserPins, userID, limit, start)
	if err != nil {
		fmt.Println("Cannot get main pins from db")
		return nil, err
	}

	result := make([]*models.Pin, 0)
	for _, row := range data {
		pin := &models.Pin{
			ID:          cast.ToInt(row[0]),
			UserID:      cast.ToInt(row[1]),
			Name:        cast.ToString(row[2]),
			Description: cast.ToString(row[3]),
			Image:       cast.ToString(row[4]),
			CreatedAt:   time.Unix(int64(cast.ToInt(row[5])), 0),
			BoardID:     cast.ToInt(row[6]),
		}
		result = append(result, pin)
	}
	return result, nil
}

func (pr *PinRepository) Update(id, userID int, pin *models.Pin) error {
	affected, err := pr.db.Exec(updatePin, pin.Name, pin.Description, id, userID)
	if err != nil {
		fmt.Printf("Cannot update pin with id: %d user id: %d\n", id, userID)
		return err
	}
	if affected == 0 {
		fmt.Printf("Cannot update pin with id: %d user id: %d\n", id, userID)
		return errors.New("Failed to find pins")
	}
	return nil
}

func (pr *PinRepository) Delete(id, userID int) error {
	affected, err := pr.db.Exec(deletePin, id, userID)
	if err != nil {
		fmt.Printf("Cannot delete pin with id: %d user id: %d\n", id, userID)
		return err
	}
	if affected == 0 {
		fmt.Printf("Cannot delete pin with id: %d user id: %d\n", id, userID)
		return errors.New("Failed to find pins")
	}
	return nil
}

func (pr *PinRepository) DeleteBoardPins(boardID int) error {
	_, err := pr.db.Exec(deleteBoardPins, boardID)
	if err != nil {
		fmt.Println("Cannot delete pins from board id: ", boardID)
		return err
	}
	return nil
}

func (pr *PinRepository) UpdateViews(id int) error {
	_, err := pr.db.Exec(updateViews, id)
	if err != nil {
		fmt.Println("Cannot find pin for updating views with id: ", id)
	}
	return err
}

func (pr *PinRepository) GetByName(name string, start int, limit int, date string, desc bool, most string) ([]*models.Pin, error) {
	var since int
	to := int(time.Now().Unix())
	switch date {
	case "":
		since = 0
	case "day":
		since = int(time.Now().AddDate(0, 0, -1).Unix())
	case "week":
		since = int(time.Now().AddDate(0, 0, -7).Unix())
	case "month":
		since = int(time.Now().AddDate(0, -1, 0).Unix())
	}

	var descStr string
	if desc {
		descStr = "desc"
	} else {
		descStr = ""
	}
	if most == "popular" {
		most = "views"
	} else {
		most = "comments"
	}

	query := fmt.Sprintf(selectPinsByName, descStr)
	data, err := pr.db.Query(query, "%"+name+"%", since, to, most, start, limit)
	if err != nil {
		fmt.Println("Failed to search database")
		return nil, err
	}
	if len(data) == 0 {
		return nil, errors.New("Could not find pins that match the given description")
	}

	result := make([]*models.Pin, 0)
	for _, row := range data {
		pin := &models.Pin{
			ID:          cast.ToInt(row[0]),
			UserID:      cast.ToInt(row[1]),
			Name:        cast.ToString(row[2]),
			Description: cast.ToString(row[3]),
			Image:       cast.ToString(row[4]),
			CreatedAt:   time.Unix(int64(cast.ToInt(row[5])), 0),
			Views:       cast.ToInt(row[6]),
			Comments:    cast.ToInt(row[7]),
			BoardID:     cast.ToInt(row[8]),
		}
		result = append(result, pin)
	}
	return result, nil
}
