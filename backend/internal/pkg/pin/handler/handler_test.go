package handler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"photosite/internal/models"
	"photosite/internal/pkg/pin/mock"
	"strings"
	"testing"
	"time"
)

type TestCaseCreate struct {
	IsAuth    bool
	Pin       *models.Pin
	PinID     int
	Response  string
	IdErr     bool
	InputErr  bool
	CreateErr bool
	Status    int
}

func TestPinHandler_CreatePin(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	tuc := mock.NewMockIPinUsecase(ctl)
	handler := PinHandler{
		pinCases: tuc,
	}

	cases := []TestCaseCreate{
		{
			Pin:      &models.Pin{},
			IsAuth:   false,
			Response: `{"message":"Pin creation failed: User has no access"}` + "\n",
			Status:   400,
		},
		{
			Pin: &models.Pin{
				UserID: 1,
			},
			IsAuth:   false,
			Response: `{"message":"Pin creation failed: User has no access"}` + "\n",
			Status:   400,
		},
		{
			IsAuth: true,
			IdErr:  true,
			Pin: &models.Pin{
				UserID: 1,
			},
			Response: `{"message":"Pin creation failed: User has no access"}` + "\n",
			Status:   400,
		},
		{
			IsAuth:   true,
			InputErr: true,
			Pin:      &models.Pin{},
			Response: `{"message":"invalid json to decode"}` + "\n",
			Status:   400,
		},
		{
			IsAuth: true,
			Pin: &models.Pin{
				UserID:      1,
				Name:        "name",
				Description: "desc",
				Image:       "imagedata",
			},
			PinID:    1,
			Response: `1` + "\n",
			Status:   200,
		},
		{
			IsAuth:    true,
			CreateErr: true,
			Pin: &models.Pin{
				UserID:      1,
				Name:        "name",
				Description: "desc",
				Image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJ" +
					"TUUH1ggDCwMADQ4NnwAAAFVJREFUGJWNkMEJADEIBEcbSDkXUnfSg" +
					"nBVeZ8LSAjiwjyEQXSFEIcHGP9oAi+H0Bymgx9MhxbFdZE2a0s9kT" +
					"Zdw01ZhhYkABSwgmf1Z6r1SNyfFf4BZ+ZUExcNUQUAAAAASUVORK5CYII=",
			},
		},
	}

	for caseNum, item := range cases {
		var r *http.Request

		if item.InputErr == false {
			jsonPin, _ := json.Marshal(item.Pin)
			r = httptest.NewRequest("POST", "/api/pins", strings.NewReader(string(jsonPin)))
		} else {
			r = httptest.NewRequest("POST", "/api/pins",
				strings.NewReader(fmt.Sprintf(`{"id":%d, "user_id:"%d, "board_id":%d, "name:""%s"", "image":"%s", "description":""%s"", "created_at":%v, "views":%d, "comments":%d}`,
					item.Pin.ID, item.Pin.UserID, item.Pin.BoardID, item.Pin.Name, item.Pin.Image, item.Pin.Description, item.Pin.CreatedAt, item.Pin.Views, item.Pin.Comments)))
		}
		r.Header.Set("Content-Type", "application/json")

		w := httptest.NewRecorder()

		ctx := r.Context()
		if item.IsAuth {
			if !item.IdErr {
				ctx = context.WithValue(ctx, "id", item.Pin.UserID)
			} else {
				ctx = context.WithValue(ctx, "id", item.Pin.UserID+1)
			}
		} else {
			ctx = context.WithValue(ctx, "id", 0)
		}
		r = r.WithContext(ctx)

		if item.IsAuth && !item.InputErr && !item.IdErr {
			err := error(nil)
			id := item.PinID
			if item.CreateErr {
				err = errors.New("")
				id = 0
			}
			tuc.EXPECT().AddPin(item.Pin).Return(id, err)
		}

		handler.CreatePin(w, r)

		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)
		bodyStr := string(body)

		if item.CreateErr {
			if w.Code == 200 || w.Code == 201 {
				t.Errorf("[%d] wrong status Response: got %+v, expected not success status",
					caseNum, w.Code)
			}
		} else {
			if bodyStr != item.Response || w.Code != item.Status {
				t.Errorf("[%d] wrong Response: got %+v, code: %d;\nexpected %+v, code: %d",
					caseNum, bodyStr, w.Code, item.Response, item.Status)
			}
		}
	}
}

type TestCaseGetById struct {
	IdErr      bool
	InputErr   bool
	IsAuth     bool
	PinToGive  *models.PinWithUser
	Err        error
	PinIDToAsk int
	UserID     int
	Status     int
	Response   string
}

func TestPinHandler_GetPinById(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	tuc := mock.NewMockIPinUsecase(ctl)
	handler := PinHandler{
		pinCases: tuc,
	}

	validPin := &models.PinWithUser{
		ID:          1,
		FullUser:      &models.User{
			ID:            1,
			Email:         "mail",
			Password:      nil,
			Login:         "login",
			About:         "nope",
			Avatar:        "yep",
			Subscriptions: 1,
			Subscribers:   1000,
		},
		BoardID:     1,
		Name:        "name",
		Image:       "img",
		Description: "desc",
		CreatedAt:   time.Now(),
		Tags:        []string{"1", "2"},
		Views:       2,
		Comments:    1,
	}
	validJson, _ := json.Marshal(validPin)
	cases := []TestCaseGetById{
		{
			IdErr:      false,
			InputErr:   false,
			IsAuth:     true,
			PinToGive:  validPin,
			Err:        nil,
			PinIDToAsk: validPin.ID,
			UserID:     1,
			Status:     200,
			Response:   string(validJson) + "\n",
		},
		{
			IdErr:      true,
			InputErr:   false,
			IsAuth:     false,
			PinToGive:  validPin,
			Err:        nil,
			PinIDToAsk: validPin.ID,
			UserID:     1,
			Status:     200,
			Response:   string(validJson) + "\n",
		},
		{
			IdErr:      false,
			InputErr:   true,
			IsAuth:     true,
			PinToGive:  validPin,
			Err:        nil,
			PinIDToAsk: validPin.ID,
			UserID:     1,
			Status:     400,
			Response:   `{"message":"Failed to find a pin: wrong path parameter"}` + "\n",
		},
		{
			IdErr:      false,
			InputErr:   false,
			IsAuth:     true,
			PinToGive:  nil,
			Err:        errors.New(""),
			PinIDToAsk: validPin.ID,
			UserID:     1,
			Status:     404,
			Response:   `{"message":"Failed to find a pin in storage"}` + "\n",
		},
		{
			IdErr:      false,
			InputErr:   false,
			IsAuth:     true,
			PinToGive:  validPin,
			Err:        errors.New(""),
			PinIDToAsk: validPin.ID,
			UserID:     1,
			Status:     404,
			Response:   `{"message":"Failed to find a pin in storage"}` + "\n",
		},
	}

	for caseNum, item := range cases {
		var r *http.Request

		target := fmt.Sprintf("/api/pins/%d", item.PinIDToAsk)
		r = httptest.NewRequest("GET", target, strings.NewReader(""))
		if item.InputErr == false {
			r = mux.SetURLVars(r, map[string]string{"id": fmt.Sprintf("%d", item.PinIDToAsk)})
		} else {
			r = mux.SetURLVars(r, map[string]string{"id": "bad"})
		}
		r.Header.Set("Content-Type", "application/json")

		w := httptest.NewRecorder()

		ctx := r.Context()
		if item.IsAuth {
			if !item.IdErr {
				ctx = context.WithValue(ctx, "id", item.UserID)
			} else {
				ctx = context.WithValue(ctx, "id", item.UserID+1)
			}
		} else {
			ctx = context.WithValue(ctx, "id", 0)
		}
		r = r.WithContext(ctx)

		if !item.InputErr {
			tuc.EXPECT().GetById(item.PinIDToAsk).Return(item.PinToGive, item.Err)
		}

		handler.GetPinById(w, r)

		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)
		bodyStr := string(body)

		if bodyStr != item.Response || w.Code != item.Status {
			t.Errorf("[%d] wrong Response: got %+v, code: %d;\nexpected %+v, code: %d",
				caseNum, bodyStr, w.Code, item.Response, item.Status)
		}
	}
}
