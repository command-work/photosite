// Слой контроллеров - обработчиков http-запросов
package handler

import (
	"fmt"
	"io"
	"net/http"
	"photosite/internal/models"
	"photosite/internal/pkg/pin"
	"photosite/internal/pkg/utils"
	"strconv"

	"github.com/gorilla/mux"
)

type PinHandler struct {
	pinCases pin.IPinUsecase
}

func NewPinHandlers(router *mux.Router, pc pin.IPinUsecase) {
	handlers := PinHandler{
		pinCases: pc,
	}

	router.HandleFunc("/pins", handlers.CreatePin).Methods(http.MethodPost, http.MethodOptions)
	router.HandleFunc("/pins", handlers.GetPinList).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/pins/{id:[0-9]+}", handlers.GetPinById).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/pins/{id:[0-9]+}", handlers.UpdatePin).Methods(http.MethodPatch, http.MethodOptions)
	router.HandleFunc("/pins/{id:[0-9]+}", handlers.DeletePin).Methods(http.MethodDelete, http.MethodOptions)
	router.HandleFunc("/boards/{id:[0-9]+}/pins", handlers.SavePin).Methods(http.MethodPost, http.MethodOptions)
}

func (ph *PinHandler) CreatePin(w http.ResponseWriter, r *http.Request) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			// ToDo: log error
		}
	}(r.Body)

	createdPin := &models.Pin{}
	if utils.DecodeJSON(w, r, createdPin) != nil {
		return
	}

	realId := r.Context().Value("id")
	if realId == 0 || realId != createdPin.UserID {
		http.Error(w, `{"message":"Pin creation failed: User has no access"}`, http.StatusBadRequest)
		return
	}

	id, err := ph.pinCases.AddPin(createdPin)
	if err != nil {
		http.Error(w, `{"message":"Pin creation failed: internal error"}`, http.StatusInternalServerError)
		return
	} else {
		if utils.EncodeJSON(w, id) != nil {
			http.Error(w, `{"message":" Failed on encoding result"}`, http.StatusInternalServerError)
		}
	}
}

func (ph *PinHandler) SavePin(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	userID := r.Context().Value("id")
	if userID == 0 {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	boardID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(r.URL.Query().Get("pin_id"))
	if err != nil {
		http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	err = ph.pinCases.SavePin(id, boardID)
	if err != nil {
		http.Error(w, `{"message":"Failed to save pin to board"}`, http.StatusBadRequest)
		return
	}

	w.Write([]byte(`{"message":"OK"}`))
}

func (ph *PinHandler) GetPinById(w http.ResponseWriter, r *http.Request) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			// ToDo: log error
		}
	}(r.Body)
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		// Should never occur since a router has a built-in regexp that only passes integers on this handler
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	foundPin, err := ph.pinCases.GetById(id)
	if err != nil {
		http.Error(w, `{"message":"Failed to find a pin in storage"}`, http.StatusNotFound)
		return
	}
	if utils.EncodeJSON(w, foundPin) != nil {
		http.Error(w, `{"message":" Failed on encoding result"}`, http.StatusInternalServerError)
	}
}

func (ph *PinHandler) GetPinList(w http.ResponseWriter, r *http.Request) {
	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		start = 0
	}

	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		limit = 10
	}

	userID := r.Context().Value("id")
	filter := r.URL.Query().Get("filter")
	var pins []*models.Pin
	switch filter {
	case "main":
		pins, err = ph.pinCases.GetMainPins(start, limit)
	case "sub":
		if userID == 0 {
			http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
			return
		}
		pins, err = ph.pinCases.GetSubPins(userID.(int), start, limit)
	case "user":
		id, err := strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
			return
		}
		pins, err = ph.pinCases.GetUserPins(id, start, limit)
	case "board":
		id, err := strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
			return
		}
		pins, err = ph.pinCases.GetBoardPins(id, start, limit)
	default:
		http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	if err != nil {
		fmt.Println("Cannot get pins list")
		http.Error(w, `{"message":"Failed to find a pins"}`, http.StatusNotFound)
		return
	}

	if utils.EncodeJSON(w, pins) != nil {
		http.Error(w, `{"message":" Failed on encoding result"}`, http.StatusInternalServerError)
	}
}

func (ph *PinHandler) UpdatePin(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	userID := r.Context().Value("id")
	if userID == 0 {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	pin := &models.Pin{}
	if utils.DecodeJSON(w, r, pin) != nil {
		return
	}

	err = ph.pinCases.UpdatePin(id, userID.(int), pin)
	if err != nil {
		fmt.Println("Cannot update pin with id", id)
		http.Error(w, `{"message":"Failed to update pin"}`, http.StatusMethodNotAllowed)
		return
	}

	utils.EncodeJSON(w, pin)
}

func (ph *PinHandler) DeletePin(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value("id")
	if userID == 0 {
		http.Error(w, `{"message":"user unathorized"}`, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, `{"message":"Failed to find a pin: wrong path parameter"}`, http.StatusBadRequest)
		return
	}

	err = ph.pinCases.DeletePin(id, userID.(int))
	if err != nil {
		fmt.Println("Cannot delete pin with id", id)
		http.Error(w, `{"message":"Failed to delete pin"}`, http.StatusMethodNotAllowed)
		return
	}

	w.Write([]byte(`{"message":"OK"}`))
}
