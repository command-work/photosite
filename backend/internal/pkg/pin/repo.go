package pin

import (
	"photosite/internal/models"
)

type IPinRepository interface {
	Create(pin *models.Pin) (int, error)
	GetById(id int) (*models.Pin, error)
	GetBoardLastPin(boardID int) (*models.Pin, error)
	GetMainPins(start, limit int) ([]*models.Pin, error)
	GetSubPins(userID, start, limit int) ([]*models.Pin, error)
	GetBoardPins(boardID, start, limit int) ([]*models.Pin, error)
	GetUserPins(userID, start, limit int) ([]*models.Pin, error)
	Update(id, userID int, pin *models.Pin) error
	Delete(id, userID int) error
	SaveToBoard(id, boardID int) error
	DeleteBoardPins(boardID int) error
	GetByName(name string, start int, limit int, date string, desc bool, most string) ([]*models.Pin, error)
	UpdateViews(id int) error
}
