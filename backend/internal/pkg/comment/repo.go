package comment

import (
	"photosite/internal/models"
)

type ICommentRepository interface {
	Create(comment *models.Comment) (int, error)
	GetById(id int) (*models.Comment, error)
	GetByPinId(id int, start int, limit int) ([]*models.Comment, error)
	GetByText(description string, start int, limit int) ([]*models.Comment, error)
}
