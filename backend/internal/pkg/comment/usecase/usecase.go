// Слой юзкейсов - бизнес-логика
package usecase

import (
	"photosite/internal/models"
	"photosite/internal/pkg/comment"
	"photosite/internal/pkg/user"
)

type CommentUsecase struct {
	commentRepo comment.ICommentRepository
	userRepo user.IUserRepository
}

func NewCommentUsecase(cr comment.ICommentRepository, ur user.IUserRepository) *CommentUsecase {
	return &CommentUsecase{
		commentRepo: cr,
		userRepo: ur,
	}
}

func (cu *CommentUsecase) AddComment(comment *models.Comment) (int, error) {
	return cu.commentRepo.Create(comment)
}

func (cu *CommentUsecase) GetById(id int) (*models.CommentWithUser, error) {
	foundComment, err := cu.commentRepo.GetById(id)
	if err == nil {
		foundUser, err := cu.userRepo.GetById(foundComment.UserID)
		if err == nil {
			res := &models.CommentWithUser{
				ID:        foundComment.ID,
				FullUser:  foundUser,
				PinID:     foundComment.PinID,
				Comment:   foundComment.Comment,
				CreatedAt: foundComment.CreatedAt,
			}
			return res, nil
		}
	}
	return nil, err
}

func (cu *CommentUsecase) GetByPinId(id int, start int, limit int) ([]*models.CommentWithUser, error) {
	var res []*models.CommentWithUser
	comments, err := cu.commentRepo.GetByPinId(id, start, limit)
	if err == nil {
		var foundUser *models.User
		for i := 0 ; i < len(comments) && err == nil; i++ {
			foundUser, err = cu.userRepo.GetById(comments[i].UserID)
			if err == nil {
				foundComment := &models.CommentWithUser{
					ID:        comments[i].ID,
					FullUser:  foundUser,
					PinID:     comments[i].PinID,
					Comment:   comments[i].Comment,
					CreatedAt: comments[i].CreatedAt,
				}
				res = append(res, foundComment)
			}
		}
	}
	return res, err
}

func (cu *CommentUsecase) GetByText(description string, start int, limit int) ([]*models.CommentWithUser, error) {
	res := make([]*models.CommentWithUser, 0)
	comments, err := cu.commentRepo.GetByText(description, start, limit)
	if err == nil {
		var foundUser *models.User
		for i := 0 ; i < len(comments) && err == nil; i++ {
			foundUser, err = cu.userRepo.GetById(comments[i].UserID)
			if err == nil {
				foundComment := &models.CommentWithUser{
					ID:        comments[i].ID,
					FullUser:  foundUser,
					PinID:     comments[i].PinID,
					Comment:   comments[i].Comment,
					CreatedAt: comments[i].CreatedAt,
				}
				res = append(res, foundComment)
			}
		}
	}
	return res, err
}