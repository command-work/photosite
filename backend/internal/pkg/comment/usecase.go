package comment

import (
	"photosite/internal/models"
)

type ICommentUsecase interface {
	AddComment(comment *models.Comment) (int, error)
	GetById(id int) (*models.CommentWithUser, error)
	GetByPinId(id int, start int, limit int) ([]*models.CommentWithUser, error)
	GetByText(description string, start int, limit int) ([]*models.CommentWithUser, error)
}
