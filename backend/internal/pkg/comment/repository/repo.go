// Слой репозиториев - для абстракции бизнес-логики от бд
package repository

import (
	"errors"
	"fmt"
	"photosite/internal/models"
	"photosite/internal/pkg/database"
	"photosite/internal/pkg/utils/cast"
	"time"
)

const (
	insertComment = "INSERT INTO comments(user_id, pin_id, comment, created_at) " +
		"VALUES($1, $2, $3, $4) RETURNING id;"
	selectComment = "SELECT id, user_id, pin_id, comment, created_at FROM comments " +
		"WHERE id = $1;"
	selectPinComments = "SELECT id, user_id, pin_id, comment, created_at FROM comments " +
		"WHERE pin_id = $1 " +
		"ORDER BY created_at " +
		"OFFSET $2 LIMIT $3;"
	selectCommentsByText = "SELECT id, user_id, pin_id, comment, created_at FROM comments " +
		"WHERE comment like $1 " +
		"ORDER BY created_at " +
		"OFFSET $2 LIMIT $3;"
)

type CommentRepository struct {
	db *database.DBManager
}

func NewCommentRepository(db *database.DBManager) *CommentRepository {
	return &CommentRepository{db: db}
}

func (cr *CommentRepository) Create(comment *models.Comment) (int, error) {
	data, err := cr.db.Query(insertComment, comment.UserID, comment.PinID, comment.Comment, time.Now().Unix())
	if err != nil {
		fmt.Printf("Failed to insert a comment %s by user with id %d to a pin with id %d in db\n",
			comment.Comment, comment.UserID, comment.PinID)
		// ToDo: better log it
		return 0, err
	}
	if len(data) == 0 {
		fmt.Printf("No id was returned by inserting a comment %s by user with id %d to a pin with id %d in db\n",
			comment.Comment, comment.UserID, comment.PinID)
		return 0, errors.New("Cannot create pin in database")
	}
	return cast.ToInt(data[0][0]), err
}

func (cr *CommentRepository) GetById(id int) (*models.Comment, error) {
	data, err := cr.db.Query(selectComment, id)
	if err != nil {
		fmt.Printf("Failed to get a comment with id=%d from db\n", id)
	}
	if len(data) == 0 {
		err = errors.New("Failed to find a comment with the given ID")
		return nil, err
	}
	res := &models.Comment{
		ID:        cast.ToInt(data[0][0]),
		UserID:    cast.ToInt(data[0][1]),
		PinID:     cast.ToInt(data[0][2]),
		Comment:   cast.ToString(data[0][3]),
		CreatedAt: time.Unix(int64(cast.ToInt(data[0][4])), 0),
	}
	return res, err
}

func (cr *CommentRepository) GetByPinId(id int, start int, limit int) ([]*models.Comment, error) {
	data, err := cr.db.Query(selectPinComments, id, start, limit)
	if err != nil {
		fmt.Printf("Failed to get a pin with id=%d from db\n", id)
	}

	res := make([]*models.Comment, 0)
	for _, row := range data {
		comment := &models.Comment{
			ID:        cast.ToInt(row[0]),
			UserID:    cast.ToInt(row[1]),
			PinID:     cast.ToInt(row[2]),
			Comment:   cast.ToString(row[3]),
			CreatedAt: time.Unix(int64(cast.ToInt(data[0][4])), 0),
		}
		res = append(res, comment)
	}

	return res, err
}

func (cr *CommentRepository) GetByText(description string, start, limit int) ([]*models.Comment, error) {
	data, err := cr.db.Query(selectCommentsByText, "%"+description+"%", start, limit)
	if err != nil {
		fmt.Printf("Failed to get comments that match the descrption: \"%s\" from db\n", description)
		return nil, err
	}
	if len(data) == 0 {
		return nil, errors.New("Could not find comments that match the given description")
	}

	res := make([]*models.Comment, 0)
	for _, row := range data {
		comment := &models.Comment{
			ID:        cast.ToInt(row[0]),
			UserID:    cast.ToInt(row[1]),
			PinID:     cast.ToInt(row[2]),
			Comment:   cast.ToString(row[3]),
			CreatedAt: time.Unix(int64(cast.ToInt(data[0][4])), 0),
		}
		res = append(res, comment)
	}

	return res, err
}