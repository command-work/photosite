package handler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"photosite/internal/models"
	"photosite/internal/pkg/comment/mock"
	"strconv"
	"strings"
	"testing"
	"time"
)

type TestCaseCreate struct {
	IsAuth    bool
	Comment   *models.Comment
	CommentId int
	Response  string
	IdErr     bool
	InputErr  bool
	CreateErr bool
	Status    int
}

func TestCommentHandler_CreateComment(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	tuc := mock.NewMockICommentUsecase(ctl)
	handler := CommentHandler{
		commentCases: tuc,
	}

	cases := []TestCaseCreate{
		{
			Comment:  &models.Comment{},
			IsAuth:   false,
			Response: `{"message":"Comment creation failed: User has no access"}` + "\n",
			Status:   400,
		},
		{
			Comment: &models.Comment{
				UserID: 1,
			},
			IsAuth:   false,
			Response: `{"message":"Comment creation failed: User has no access"}` + "\n",
			Status:   400,
		},
		{
			IsAuth: true,
			IdErr:  true,
			Comment: &models.Comment{
				UserID: 1,
			},
			Response: `{"message":"Comment creation failed: User has no access"}` + "\n",
			Status:   400,
		},
		{
			IsAuth:   true,
			InputErr: true,
			Comment:  &models.Comment{},
			Response: `{"message":"invalid json to decode"}` + "\n",
			Status:   400,
		},
		{
			IsAuth: true,
			Comment: &models.Comment{
				UserID:  1,
				PinID:   1,
				Comment: "comment",
			},
			CommentId: 1,
			Response:  `1` + "\n",
			Status:    200,
		},
		{
			IsAuth:    true,
			CreateErr: true,
			Comment: &models.Comment{
				UserID:  1,
				PinID:   1,
				Comment: "comment",
			},
		},
	}

	for caseNum, item := range cases {
		var r *http.Request

		target := fmt.Sprintf("/api/pins/%d/comments", item.Comment.PinID)
		if item.InputErr == false {
			jsonComment, _ := json.Marshal(item.Comment)
			r = httptest.NewRequest("POST", target, strings.NewReader(string(jsonComment)))
		} else {
			r = httptest.NewRequest("POST", target,
				strings.NewReader(fmt.Sprintf(`{"id":%d, "user_id:"%d, "pin_id":%d, "comment:""%s"", "created_at":%v}`,
					item.Comment.ID, item.Comment.UserID, item.Comment.PinID, item.Comment.Comment, item.Comment.CreatedAt)))
		}
		r = mux.SetURLVars(r, map[string]string{"pin_id": fmt.Sprintf("%d", item.Comment.PinID)})

		r.Header.Set("Content-Type", "application/json")

		w := httptest.NewRecorder()

		ctx := r.Context()
		if item.IsAuth {
			if !item.IdErr {
				ctx = context.WithValue(ctx, "id", item.Comment.UserID)
			} else {
				ctx = context.WithValue(ctx, "id", item.Comment.UserID+1)
			}
		} else {
			ctx = context.WithValue(ctx, "id", 0)
		}
		r = r.WithContext(ctx)

		if item.IsAuth && !item.InputErr && !item.IdErr {
			err := error(nil)
			id := item.CommentId
			if item.CreateErr {
				err = errors.New("")
				id = 0
			}
			tuc.EXPECT().AddComment(item.Comment).Return(id, err)
		}

		handler.CreateComment(w, r)

		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)
		bodyStr := string(body)

		if item.CreateErr {
			if w.Code == 200 || w.Code == 201 {
				t.Errorf("[%d] wrong status Response: got %+v, expected not success status",
					caseNum, w.Code)
			}
		} else {
			if bodyStr != item.Response || w.Code != item.Status {
				t.Errorf("[%d] wrong Response: got %+v, code: %d;\nexpected %+v, code: %d",
					caseNum, bodyStr, w.Code, item.Response, item.Status)
			}
		}
	}
}

type TestCaseGetById struct {
	IdErr          bool
	InputErr       bool
	IsAuth         bool
	CommentToGive  *models.CommentWithUser
	Err            error
	PinIDToAsk     int
	CommentIdToAsk int
	UserID         int
	Status         int
	Response       string
}

func TestCommentHandler_GetCommentById(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	tuc := mock.NewMockICommentUsecase(ctl)
	handler := CommentHandler{
		commentCases: tuc,
	}

	validComment := &models.CommentWithUser{
		ID: 1,
		FullUser: &models.User{
			ID:            1,
			Email:         "mail",
			Password:      nil,
			Login:         "login",
			About:         "nope",
			Avatar:        "yep",
			Subscriptions: 1,
			Subscribers:   1000,
		},
		PinID:     1,
		Comment:   "comment",
		CreatedAt: time.Now(),
	}
	validJson, _ := json.Marshal(validComment)
	cases := []TestCaseGetById{
		{
			IdErr:          false,
			InputErr:       false,
			IsAuth:         true,
			CommentToGive:  validComment,
			Err:            nil,
			CommentIdToAsk: 1,
			PinIDToAsk:     1,
			UserID:         1,
			Status:         200,
			Response:       string(validJson) + "\n",
		},
		{
			IdErr:          true,
			InputErr:       false,
			IsAuth:         false,
			CommentToGive:  validComment,
			Err:            nil,
			CommentIdToAsk: 1,
			PinIDToAsk:     1,
			UserID:         1,
			Status:         200,
			Response:       string(validJson) + "\n",
		},
		{
			IdErr:          false,
			InputErr:       true,
			IsAuth:         true,
			CommentToGive:  validComment,
			Err:            nil,
			CommentIdToAsk: 1,
			PinIDToAsk:     1,
			UserID:         1,
			Status:         400,
			Response:       `{"message":"Failed to find a comment: wrong path parameter"}` + "\n",
		},
		{
			IdErr:          false,
			InputErr:       false,
			IsAuth:         true,
			CommentToGive:  nil,
			Err:            errors.New(""),
			CommentIdToAsk: 1,
			PinIDToAsk:     1,
			UserID:         1,
			Status:         404,
			Response:       `{"message":" Could not find a comment with the given ID in storage"}` + "\n",
		},
		{
			IdErr:          false,
			InputErr:       false,
			IsAuth:         true,
			CommentToGive:  validComment,
			Err:            errors.New(""),
			CommentIdToAsk: 1,
			PinIDToAsk:     1,
			UserID:         1,
			Status:         404,
			Response:       `{"message":" Could not find a comment with the given ID in storage"}` + "\n",
		},
		{
			IdErr:          false,
			InputErr:       false,
			IsAuth:         true,
			CommentToGive:  validComment,
			Err:            nil,
			CommentIdToAsk: 1,
			PinIDToAsk:     2,
			UserID:         1,
			Status:         400,
			Response:       `{"message":" Requested comment does not belong to the given pin"}` + "\n",
		},
	}

	for caseNum, item := range cases {
		var r *http.Request

		target := fmt.Sprintf("/api/pins/%d/comments/%d", item.PinIDToAsk, item.CommentIdToAsk)
		r = httptest.NewRequest("GET", target, strings.NewReader(""))
		if item.InputErr == false {
			r = mux.SetURLVars(r, map[string]string{"pin_id": fmt.Sprintf("%d", item.PinIDToAsk), "id": fmt.Sprintf("%d", item.CommentIdToAsk)})
		} else {
			r = mux.SetURLVars(r, map[string]string{"pin_id": "bad", "id": "bad"})
		}

		r.Header.Set("Content-Type", "application/json")

		w := httptest.NewRecorder()

		ctx := r.Context()
		if item.IsAuth {
			if !item.IdErr {
				ctx = context.WithValue(ctx, "id", item.UserID)
			} else {
				ctx = context.WithValue(ctx, "id", item.UserID+1)
			}
		} else {
			ctx = context.WithValue(ctx, "id", 0)
		}
		r = r.WithContext(ctx)

		if !item.InputErr {
			tuc.EXPECT().GetById(item.CommentIdToAsk).Return(item.CommentToGive, item.Err)
		}

		handler.GetCommentById(w, r)

		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)
		bodyStr := string(body)

		if bodyStr != item.Response || w.Code != item.Status {
			t.Errorf("[%d] wrong Response: got %+v, code: %d;\nexpected %+v, code: %d",
				caseNum, bodyStr, w.Code, item.Response, item.Status)
		}
	}
}

type GetComments struct {
	IdErr          bool
	WrongPinId     bool
	WrongStart     bool
	WrongLimit     bool
	IsAuth         bool
	CommentsToGive []*models.CommentWithUser
	Err            error
	PinIdToAsk     int
	UserID         int
	start          int
	limit          int
	Status         int
	Response       string
}

func TestCommentHandler_GetComments(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	tuc := mock.NewMockICommentUsecase(ctl)
	handler := CommentHandler{
		commentCases: tuc,
	}

	validComments := []*models.CommentWithUser{
		{
			ID: 1,
			FullUser: &models.User{
				ID:            1,
				Email:         "mail",
				Password:      nil,
				Login:         "login",
				About:         "nope",
				Avatar:        "yep",
				Subscriptions: 1,
				Subscribers:   1000,
			},
			PinID:     1,
			Comment:   "comment",
			CreatedAt: time.Now(),
		},
	}
	validJson, _ := json.Marshal(validComments)
	cases := []GetComments{
		{
			IdErr:          false,
			WrongPinId:     false,
			WrongStart:     false,
			WrongLimit:     false,
			IsAuth:         true,
			CommentsToGive: validComments,
			Err:            nil,
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         200,
			Response:       string(validJson) + "\n",
		},
		{
			IdErr:          true,
			WrongPinId:     false,
			WrongStart:     false,
			WrongLimit:     false,
			IsAuth:         false,
			CommentsToGive: validComments,
			Err:            nil,
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         200,
			Response:       string(validJson) + "\n",
		},
		{
			IdErr:          false,
			WrongPinId:     true,
			WrongStart:     false,
			WrongLimit:     false,
			IsAuth:         true,
			CommentsToGive: validComments,
			Err:            nil,
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         400,
			Response:       `{"message":"Failed to get comments: wrong path parameter"}` + "\n",
		},
		{
			IdErr:          false,
			WrongPinId:     false,
			WrongStart:     true,
			WrongLimit:     false,
			IsAuth:         true,
			CommentsToGive: validComments,
			Err:            nil,
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         400,
			Response:       `{"message":"Failed to get comments: wrong query parameter: start"}` + "\n",
		},
		{
			IdErr:          false,
			WrongPinId:     false,
			WrongStart:     false,
			WrongLimit:     true,
			IsAuth:         true,
			CommentsToGive: validComments,
			Err:            nil,
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         400,
			Response:       `{"message":"Failed to get comments: wrong query parameter: limit"}` + "\n",
		},
		{
			IdErr:          false,
			WrongPinId:     false,
			WrongStart:     false,
			WrongLimit:     false,
			IsAuth:         true,
			CommentsToGive: nil,
			Err:            errors.New(""),
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         404,
			Response:       `{"message":" Could not get comments from storage for a pin with the given ID"}` + "\n",
		},
		{
			IdErr:          false,
			WrongPinId:     false,
			WrongStart:     false,
			WrongLimit:     false,
			IsAuth:         true,
			CommentsToGive: validComments,
			Err:            errors.New(""),
			PinIdToAsk:     1,
			UserID:         1,
			start:          1,
			limit:          1,
			Status:         404,
			Response:       `{"message":" Could not get comments from storage for a pin with the given ID"}` + "\n",
		},
	}

	for caseNum, item := range cases {
		var r *http.Request

		query := fmt.Sprintf("/api/pins/%d/comments?", item.PinIdToAsk)
		if !item.WrongStart {
			query += "start=" + strconv.Itoa(item.start) + "&"
		} else {
			query += "start=bad&"
		}
		if !item.WrongLimit {
			query += "limit=" + strconv.Itoa(item.limit)
		} else {
			query += "limit=bad"
		}
		r = httptest.NewRequest("GET", query, strings.NewReader(""))
		if !item.WrongPinId {
			r = mux.SetURLVars(r, map[string]string{"pin_id": fmt.Sprintf("%d", item.PinIdToAsk)})
		} else {
			r = mux.SetURLVars(r, map[string]string{"pin_id": "bad"})
		}


		r.Header.Set("Content-Type", "application/json")

		w := httptest.NewRecorder()

		ctx := r.Context()
		if item.IsAuth {
			if !item.IdErr {
				ctx = context.WithValue(ctx, "id", item.UserID)
			} else {
				ctx = context.WithValue(ctx, "id", item.UserID+1)
			}
		} else {
			ctx = context.WithValue(ctx, "id", 0)
		}
		r = r.WithContext(ctx)

		if !item.WrongPinId && !item.WrongStart && !item.WrongLimit {
			tuc.EXPECT().GetByPinId(item.PinIdToAsk, item.start, item.limit).Return(item.CommentsToGive, item.Err)
		}

		handler.GetComments(w, r)

		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)
		bodyStr := string(body)

		if bodyStr != item.Response || w.Code != item.Status {
			t.Errorf("[%d] wrong Response: got %+v, code: %d;\nexpected %+v, code: %d",
				caseNum, bodyStr, w.Code, item.Response, item.Status)
		}
	}
}
