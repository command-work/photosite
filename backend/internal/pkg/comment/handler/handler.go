// Слой контроллеров - обработчиков http-запросов
package handler

import (
	"io"
	"net/http"
	"photosite/internal/models"
	"photosite/internal/pkg/comment"
	"photosite/internal/pkg/utils"
	"strconv"

	"github.com/gorilla/mux"
)

type CommentHandler struct {
	commentCases comment.ICommentUsecase
}

func NewCommentHandlers(router *mux.Router, pc comment.ICommentUsecase) {
	handlers := CommentHandler{
		commentCases: pc,
	}

	router.HandleFunc("/pins/{pin_id:[0-9]+}/comments", handlers.CreateComment).Methods(http.MethodPost, http.MethodOptions)
	router.HandleFunc("/pins/{pin_id:[0-9]+}/comments", handlers.GetComments).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/pins/{pin_id:[0-9]+}/comments/{id:[0-9]+}", handlers.GetCommentById).Methods(http.MethodGet, http.MethodOptions)
}

func (ph *CommentHandler) CreateComment(w http.ResponseWriter, r *http.Request) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			// ToDo: log error
		}
	}(r.Body)

	createdComment := &models.Comment{}
	if utils.DecodeJSON(w, r, createdComment) != nil {
		return
	}

	realId := r.Context().Value("id")
	if realId == 0 || realId != createdComment.UserID {
		http.Error(w, `{"message":"Comment creation failed: User has no access"}`, http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["pin_id"])
	if err != nil {
		// Should never occur since a router has a built-in regexp that only passes integers on this handler
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to create a comment: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	createdComment.PinID = id

	id, err = ph.commentCases.AddComment(createdComment)
	if err != nil {
		http.Error(w, `{"message":"CommentWithUser creation failed: internal error"}`, http.StatusBadRequest)
		return
	} else {
		err = utils.EncodeJSON(w, id)
	}
}

func (ph *CommentHandler) GetComments(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["pin_id"])
	if err != nil {
		// Should never occur since a router has a built-in regexp that only passes integers on this handler
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to get comments: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to get comments: wrong query parameter: start"}`, http.StatusBadRequest)
		return
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to get comments: wrong query parameter: limit"}`, http.StatusBadRequest)
		return
	}
	foundComments, err := ph.commentCases.GetByPinId(id, start, limit)
	if err != nil {
		http.Error(w, `{"message":" Could not get comments from storage for a pin with the given ID"}`, http.StatusNotFound)
		return
	}
	_ = utils.EncodeJSON(w, foundComments)
}

func (ph *CommentHandler) GetCommentById(w http.ResponseWriter, r *http.Request) {
	var comment_id int
	vars := mux.Vars(r)
	pin_id, err := strconv.Atoi(vars["pin_id"])
	if err == nil {
		comment_id, err = strconv.Atoi(vars["id"])
	}
	if err != nil {
		// Should never occur since a router has a built-in regexp that only passes integers on this handler
		// ToDo: Log error
		http.Error(w, `{"message":"Failed to find a comment: wrong path parameter"}`, http.StatusBadRequest)
		return
	}
	foundComment, err := ph.commentCases.GetById(comment_id)
	if err != nil {
		http.Error(w, `{"message":" Could not find a comment with the given ID in storage"}`, http.StatusNotFound)
		return
	}
	if foundComment.PinID != pin_id {
		http.Error(w, `{"message":" Requested comment does not belong to the given pin"}`, http.StatusBadRequest)
		return
	}
	_ = utils.EncodeJSON(w, foundComment)
}
