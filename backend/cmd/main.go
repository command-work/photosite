// Точка входа в бэкенд приложения
package main

import (
	"flag"
	"photosite/internal/pkg/server"
)

func main() {
	var docker bool
	flag.BoolVar(&docker, "docker", false, "flag if running in docker container")
	flag.Parse()

	server.RunServer("0.0.0.0:31337", docker)
}
